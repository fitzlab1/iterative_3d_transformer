/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import Jama.Matrix;
import pointpatternmatcher_.Constants;
import utils.HashCodeUtil;

/**
 *
 * @author Theo
 */
public class Point3D {
    public double x;
    public double y;
    public double z;
    
    public Point3D(){
        x=0;
        y=0;
        z=0;
    }
    
    public Point3D(double[] xyz){
        x=xyz[0];
        y=xyz[1];
        z=xyz[2];
    }
    
    public void flip(){
        x = -x;
        y = -y;
        z = -z;
    }
    
    public double getDistance(Point3D q){
        return Math.sqrt(
                Math.pow((x-q.x),2) +  
                Math.pow((y-q.y),2) +
                Math.pow((z-q.z),2));
    }
    
    public Point3D subtract(Point3D q){
        //result = this point - q
        Point3D result = new Point3D();
        result.x = x - q.x;
        result.y = y - q.y;
        result.z = z - q.z;
        return result;
    }
    
    public Point3D add(Point3D q){
        //result = this point - q
        Point3D result = new Point3D();
        result.x = x + q.x;
        result.y = y + q.y;
        result.z = z + q.z;
        return result;
    }
    
    public Point3D copy(){
        return new Point3D(new double[]{x,y,z});
    }
    
    public Point3D computeCrossProduct(Point3D q){
        //crosses this point (vector) with q
        Point3D crossProduct = new Point3D();

        crossProduct.x = y * q.z - z * q.y;
        crossProduct.y = z * q.x - x * q.z;
        crossProduct.z = x * q.y - y * q.x;

        return crossProduct;
    }
    
    public static double[] computeCrossProduct (double[] v0, double[] v1){
        double crossProduct[] = new double[3];

        crossProduct[0] = v0[1] * v1[2] - v0[2] * v1[1];
        crossProduct[1] = v0[2] * v1[0] - v0[0] * v1[2];
        crossProduct[2] = v0[0] * v1[1] - v0[1] * v1[0];

        return crossProduct;
    }
    
    public double angleBetween(Point3D q){
        //result = angle between this point and q
        //(Points are vectors in this case, same thing, whatever man.)
        
        double dotProduct = q.x*x + q.y*y + q.z*z;
        double magnitudeP = Math.sqrt(x*x+y*y+z*z);
        double magnitudeQ = Math.sqrt(q.x*q.x+q.y*q.y+q.z*q.z);
        
        return Math.acos(dotProduct / (magnitudeP*magnitudeQ))*180.0/Math.PI;
    }
    
    public double getMagnitude(){
        return Math.sqrt(x*x + y*y + z*z);
    }
    
    public Point3D toUnitVector(){
        double magnitude = getMagnitude();
        Point3D u = new Point3D();
        u.x = x / magnitude;
        u.y = y / magnitude;
        u.z = z / magnitude;
        return u;
    }
    
    public Matrix toMatrix(boolean paddedRight){
        //paddedRight: Do we put a column of ones to the right of the point?
        //this is usually a good thing to do - necessary for calculating transforms
        //between matrices, that sort of thing.
        int width = 3;
        if(paddedRight){
            width = 4;
        }
        double[][] array = new double[1][width];
        array[0][0] = x;
        array[0][1] = y;
        array[0][2] = z;
        if(paddedRight){
            array[0][3] = 1;
        }
        return new Matrix(array);
    }
    
    @Override
    public String toString(){
        //return "x: " + x + " y: " + y + " z: " + z;
        return "" + x + " " + y + " " + z;
    }
    
    public static Matrix pointArrayToMatrix(Point3D[] points, boolean paddedRight){
        //paddedRight: Do we put a column of ones to the right of the points?
        //this is usually a good thing to do - necessary for calculating transforms
        //between matrices, that sort of thing.
        int width = 3;
        if(paddedRight){
            width = 4;
        }
        double[][] array = new double[points.length][width];
        for(int i = 0; i < points.length; i++){
            array[i][0] = points[i].x;
            array[i][1] = points[i].y;
            array[i][2] = points[i].z;
            if(paddedRight){
                array[i][3] = 1;
            }
        }
        return new Matrix(array);
    }
    
    public static Matrix pointArrayToMatrix2D(Point3D[] points, boolean paddedRight){
        //paddedRight: Do we put a column of ones to the right of the points?
        //this is usually a good thing to do - necessary for calculating transforms
        //between matrices, that sort of thing.
        
        //This 2D version shaves off the Z-coordinate.
        int width = 2;
        if(paddedRight){
            width = 3;
        }
        double[][] array = new double[points.length][width];
        for(int i = 0; i < points.length; i++){
            array[i][0] = points[i].x;
            array[i][1] = points[i].y;
            if(paddedRight){
                array[i][2] = 1;
            }
        }
        return new Matrix(array);
    }
    
    public static Point3D[] matrixToPointArray(Matrix mat){
        Point3D[] points = new Point3D[mat.getRowDimension()];
        for(int i = 0; i < mat.getRowDimension(); i++){
            double x = mat.get(i, 0);
            double y = mat.get(i, 1);
            double z = mat.get(i, 2);
            
            points[i] = new Point3D(new double[]{x,y,z});
        }
        return points;
    }
    
    public Point3D times(double magnitude){
        //multiplies a vector by some magnitude
        return new Point3D(new double[]{x*magnitude, y*magnitude, z*magnitude});
    }
    
    public double dot(Point3D vec){
        return (vec.x*x+vec.y*y+vec.z*z);
    }
    
    public boolean isParallelTo(Point3D vec){
        Point3D crossProduct = computeCrossProduct(vec);
        if(crossProduct.getMagnitude() < Constants.EPSILON)
            return true;
        else
            return false;
    }
    
    public static Point3D getAverage(Point3D[] points){
        //computes a rolling average for each coordinate to avoid overflows
        //see: http://en.wikipedia.org/wiki/Moving_average#Cumulative_moving_average
        
        Point3D avg = new Point3D(new double[]{0,0,0});
        for(int i = 0; i < points.length; i++){
            avg.x = (avg.x*i + points[i].x)/(i+1);
            avg.y = (avg.y*i + points[i].y)/(i+1);
            avg.z = (avg.z*i + points[i].z)/(i+1);
        }
        return avg;
    }
    
    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Point3D))
            return false;
        Point3D q = (Point3D) o;        
        if(x!=q.x || y!=q.y || z!=q.z)
            return false;
        else
            return true;
    }
    
    public boolean isExtremelyCloseTo(Point3D p){
        //more robust test than equals(). Allows for rounding errors.
        if(Math.abs(p.x - x) < Constants.EPSILON &&
            Math.abs(p.y - y) < Constants.EPSILON &&
            Math.abs(p.z - z) < Constants.EPSILON){
            return true;
        }
        else
            return false;
    }
    
    @Override public int hashCode(){
        int result = HashCodeUtil.SEED;
        result = HashCodeUtil.hash(result, x);
        result = HashCodeUtil.hash(result, y);
        result = HashCodeUtil.hash(result, z);
        return result;
    }

    
}
