package geometry;

import transforms.AffineTransform3D;
import Jama.Matrix;

public class Tetrahedron {
    public Point3D[] points; //there will be 4, ordered: {a, b, c, d}.
    public Edge3D[] edges; //there will be 6, ordered: {ab, ac, ad, bc, bd, cd}. 
    public Plane3D[] faces; //there will be 4, ordered: {bcd, acd, abd, abc}. Normals point outwards.
   
    public Tetrahedron(Point3D[] points_){
        points = points_;
        
        calcEdges();
        calcFaces();
    }
    
    public void calcEdges(){
        edges = new Edge3D[6];
        int e = 0; //edge index
        for(int f = 0; f < points.length; f++){
            for(int g = f+1; g < points.length; g++){
                edges[e] = new Edge3D(new Point3D[] {points[f], points[g]});
                e++;
            }
        }
    }
    
    public void calcFaces(){
        faces = new Plane3D[4];
        for(int i = 0; i < points.length; i++){
            Point3D[] facePoints = new Point3D[3];
            int index = 0;
            for(int j = 0; j < points.length; j++){
                if(j==i){
                    continue;
                }
                facePoints[index] = points[j];
                index++;
            }
            Plane3D face = new Plane3D(facePoints);
            
            //make normal point outwards if needed
            if(!(face.normalPointsAwayFrom(points[i]))){
                face.flipNormal();
            }
            faces[i] = face;
        }
    }
    
    public boolean containsPoint(Point3D p){
        //returns whether this tetrahedron contains p.
        //Note that points are considered inside a tetra if they are
        //on the tetra's corners or faces (or are inside the volume).
        for(int i = 0; i < faces.length; i++){
            //for the point to be inside this face, either the normal
            //has to direct away from it or the point has to be exactly
            //on the face
            if(! (faces[i].normalPointsAwayFrom(p) || faces[i].isOnPlane(p))){
                //neither are true, so the point is outside this face
                return false;
            }
        }
        //point was inside all faces
        return true;
    }
    
    public AffineTransform3D affineTransformTo(Tetrahedron u){
        //returns the affine transform needed to turn this tetra into
        //tetrahedron u
        //assumes t.points[i] corresponds to u.points[i] for i=[0..3].
        Matrix tMat = Point3D.pointArrayToMatrix(points, true);
        Matrix uMat = Point3D.pointArrayToMatrix(u.points, true);
        AffineTransform3D tform = null;
        try{
            tform = new AffineTransform3D(tMat,uMat);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }        
        return tform;
    }    
    
    /*
     * so, yeah, this is invalid!
    public double angleBetween(Tetrahedron u){
        return affineTransformTo(u).getZRotationAngle();
    }
    
     */
    public double getVolume(){
        //set up 3x3 matrix of a-b,b-c,c-d
        //take 1/6 of the determinant to get the volume
        double[][] mat = new double[3][3];
        Point3D a = edges[0].points[0];
        Point3D b = edges[3].points[0];
        Point3D c = edges[5].points[0];
        Point3D d = edges[5].points[1];        
        
        mat[0][0] = a.x-b.x;
        mat[0][1] = a.y-b.y;
        mat[0][2] = a.z-b.z;
        
        mat[1][0] = b.x-c.x;
        mat[1][1] = b.y-c.y;
        mat[1][2] = b.z-c.z;
        
        mat[2][0] = c.x-d.x;
        mat[2][1] = c.y-d.y;
        mat[2][2] = c.z-d.z;
        
        Matrix m = new Matrix(mat);
        return Math.abs(m.det()*1/6);
    }
    
    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Tetrahedron))
            return false;
        Tetrahedron t = (Tetrahedron) o;
        
        for(int i = 0 ; i < points.length; i++){
            if(! points[i].equals(t.points[i]))
                return false;
        }
        return true;
    }
        
    @Override public int hashCode(){
        //hashcodes are commutative: equal tetras will hash to the same spot.
        //vertices are in arbitrary order.
        //Allows for a quick hash-search to find matching faces.
        int result = 0;
        for(int i = 0 ; i < points.length; i++){
            result += points[i].hashCode();
        }
        return result;
    }
    
    
    public static void print(String s){
        System.out.println(s);
    }
    
}
