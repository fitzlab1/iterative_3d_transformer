package transformandoverlay_;

import Jama.Matrix;
import geometry.Box3D;
import transforms.AffineTransform2D;
import transforms.AffineTransform3D;
import utils.MatrixMethods;
import geometry.Point3D;
import transforms.Transform;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JFrame;


import ij.*;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.io.FileSaver;
import ij.plugin.frame.*;
import ij.process.ImageProcessor;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.awt.List;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import pointpatternmatcher_.Constants;
import transforms.AffineInfo;
import transforms.PiecewiseAffineTransform2D;
import transforms.PiecewiseAffineTransform3D;
import utils.FileAndDirOperations;

/**
 * The application's main frame.
 */
public class TransformAndOverlay_View extends FrameView {
    ImageJ imageJ;
    RoiManager Manager = null;

    PointsTableModel srcPtsTable = new PointsTableModel();
    PointsTableModel destPtsTable = new PointsTableModel();

    //what ImageJ windows are open? And which ones are selected?
    WindowListModel windowListSrcPts = new WindowListModel();
    WindowListModel windowListDestPts = new WindowListModel();
    WindowListModel windowListSrcTransformPts = new WindowListModel();

    WindowListModel windowListSrcTransform = new WindowListModel();
    WindowListModel windowListDestTransform = new WindowListModel();

    //What transform has been applied to the source image to get it here?
    Transform prevSrcTransform = null;

    public TransformAndOverlay_View(SingleFrameApplication app) {

        super(app);
        initComponents();

        //back to netbeans bullshit


        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = TransformAndOverlayApp_.getApplication().getMainFrame();
            aboutBox = new TransformAndOverlay_AboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        TransformAndOverlayApp_.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        btnSavePoints = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnLoadPoints = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jComboBox5 = new javax.swing.JComboBox();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtTransformInfo = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        radioLSAffine = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        txtTransformedOutDir = new javax.swing.JTextField();
        btnTransform = new javax.swing.JButton();
        chkWriteOutImages = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox();
        jComboBox4 = new javax.swing.JComboBox();
        radioTranslateDest = new javax.swing.JRadioButton();
        radioCropToDest = new javax.swing.JRadioButton();
        jLabel14 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtHelp = new javax.swing.JTextArea();
        btnRefreshPoints = new javax.swing.JButton();
        btnRefreshWindowList = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTabbedPane1.setName("jTabbedPane1"); // NOI18N

        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(transformandoverlay_.TransformAndOverlayApp_.class).getContext().getResourceMap(TransformAndOverlay_View.class);
        btnSavePoints.setText(resourceMap.getString("btnSavePoints.text")); // NOI18N
        btnSavePoints.setName("btnSavePoints"); // NOI18N
        btnSavePoints.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSavePointsActionPerformed(evt);
            }
        });
        jPanel2.add(btnSavePoints, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 610, 145, 34));

        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, 210, 20));

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jTable1.setModel(srcPtsTable);
        jTable1.setName("jTable1"); // NOI18N
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jTable1);

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 170, 270));

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        jTable2.setModel(destPtsTable);
        jTable2.setName("jTable2"); // NOI18N
        jTable2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(jTable2);

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 160, 170, 270));

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 130, 120, 20));

        jLabel8.setText(resourceMap.getString("jLabel8.text")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 100, 20));

        btnLoadPoints.setText(resourceMap.getString("btnLoadPoints.text")); // NOI18N
        btnLoadPoints.setName("btnLoadPoints"); // NOI18N
        btnLoadPoints.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadPointsActionPerformed(evt);
            }
        });
        jPanel2.add(btnLoadPoints, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 610, 145, 34));

        jLabel12.setText(resourceMap.getString("jLabel12.text")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 290, 20));

        jComboBox1.setModel(windowListDestPts);
        jComboBox1.setName("jComboBox1"); // NOI18N
        jPanel2.add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 100, 110, -1));

        jComboBox2.setModel(windowListSrcPts);
        jComboBox2.setName("jComboBox2"); // NOI18N
        jPanel2.add(jComboBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 110, -1));

        jLabel13.setText(resourceMap.getString("jLabel13.text")); // NOI18N
        jLabel13.setName("jLabel13"); // NOI18N
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 210, 20));

        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 210, 20));

        jComboBox5.setModel(windowListSrcTransformPts);
        jComboBox5.setName("jComboBox5"); // NOI18N
        jPanel2.add(jComboBox5, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 70, 110, -1));

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        txtTransformInfo.setColumns(20);
        txtTransformInfo.setLineWrap(true);
        txtTransformInfo.setRows(5);
        txtTransformInfo.setText(resourceMap.getString("txtTransformInfo.text")); // NOI18N
        txtTransformInfo.setWrapStyleWord(true);
        txtTransformInfo.setName("txtTransformInfo"); // NOI18N
        jScrollPane4.setViewportView(txtTransformInfo);

        jPanel2.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 450, 350, 140));

        jTabbedPane1.addTab(resourceMap.getString("jPanel2.TabConstraints.tabTitle"), jPanel2); // NOI18N

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        buttonGroup1.add(radioLSAffine);
        radioLSAffine.setSelected(true);
        radioLSAffine.setText(resourceMap.getString("radioLSAffine.text")); // NOI18N
        radioLSAffine.setName("radioLSAffine"); // NOI18N
        jPanel3.add(radioLSAffine, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 210, -1));

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText(resourceMap.getString("jRadioButton2.text")); // NOI18N
        jRadioButton2.setName("jRadioButton2"); // NOI18N
        jPanel3.add(jRadioButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 180, -1));

        txtTransformedOutDir.setText(resourceMap.getString("txtTransformedOutDir.text")); // NOI18N
        txtTransformedOutDir.setName("txtTransformedOutDir"); // NOI18N
        jPanel3.add(txtTransformedOutDir, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 520, 250, -1));

        btnTransform.setText(resourceMap.getString("btnTransform.text")); // NOI18N
        btnTransform.setName("btnTransform"); // NOI18N
        btnTransform.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTransformActionPerformed(evt);
            }
        });
        jPanel3.add(btnTransform, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 440, 130, 30));

        chkWriteOutImages.setText(resourceMap.getString("chkWriteOutImages.text")); // NOI18N
        chkWriteOutImages.setName("chkWriteOutImages"); // NOI18N
        jPanel3.add(chkWriteOutImages, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 490, 230, -1));

        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 340, 200, 20));

        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, 210, 20));

        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N
        jPanel3.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 210, 20));

        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 290, 20));

        jComboBox3.setModel(windowListSrcTransform);
        jComboBox3.setName("jComboBox3"); // NOI18N
        jPanel3.add(jComboBox3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 40, 110, -1));

        jComboBox4.setModel(windowListDestTransform);
        jComboBox4.setName("jComboBox4"); // NOI18N
        jPanel3.add(jComboBox4, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, 110, -1));

        buttonGroup3.add(radioTranslateDest);
        radioTranslateDest.setText(resourceMap.getString("radioTranslateDest.text")); // NOI18N
        radioTranslateDest.setName("radioTranslateDest"); // NOI18N
        jPanel3.add(radioTranslateDest, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 400, 350, -1));

        buttonGroup3.add(radioCropToDest);
        radioCropToDest.setSelected(true);
        radioCropToDest.setText(resourceMap.getString("radioCropToDest.text")); // NOI18N
        radioCropToDest.setName("radioCropToDest"); // NOI18N
        jPanel3.add(radioCropToDest, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 370, 330, -1));

        jLabel14.setText(resourceMap.getString("jLabel14.text")); // NOI18N
        jLabel14.setName("jLabel14"); // NOI18N
        jPanel3.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 200, 20));

        jTabbedPane1.addTab(resourceMap.getString("jPanel3.TabConstraints.tabTitle"), jPanel3); // NOI18N

        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        txtHelp.setColumns(20);
        txtHelp.setRows(5);
        txtHelp.setName("txtHelp"); // NOI18N
        jScrollPane2.setViewportView(txtHelp);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 360, 550));

        jTabbedPane1.addTab(resourceMap.getString("jPanel1.TabConstraints.tabTitle"), jPanel1); // NOI18N

        mainPanel.add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 410, 680));

        btnRefreshPoints.setText(resourceMap.getString("btnRefreshPoints.text")); // NOI18N
        btnRefreshPoints.setName("btnRefreshPoints"); // NOI18N
        btnRefreshPoints.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshPointsActionPerformed(evt);
            }
        });
        mainPanel.add(btnRefreshPoints, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 80, 130, 30));

        btnRefreshWindowList.setText(resourceMap.getString("btnRefreshWindowList.text")); // NOI18N
        btnRefreshWindowList.setName("btnRefreshWindowList"); // NOI18N
        btnRefreshWindowList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshWindowListActionPerformed(evt);
            }
        });
        mainPanel.add(btnRefreshWindowList, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 40, 130, 30));

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(transformandoverlay_.TransformAndOverlayApp_.class).getContext().getActionMap(TransformAndOverlay_View.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 613, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 443, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

private Point3D invertPoint(Point3D point){

    //uses prevSrcTransform to invert a point
    Matrix invertedPointMat;

    if(prevSrcTransform.getClass().equals(AffineTransform2D.class) ||
            prevSrcTransform.getClass().equals(PiecewiseAffineTransform2D.class)){
        //the 2D inversion can be a little weird. First we need a 2D point matrix:
        Matrix pointMat = Point3D.pointArrayToMatrix2D(new Point3D[]{point}, true);
        invertedPointMat = prevSrcTransform.applyInverseToPoints(pointMat);

        //But now we've lost any stack information we had in the Z-coordinate. Recover that.
        invertedPointMat.set(0, 2, point.z);
    }
    else{
        //the 3D inverse is easy
        Matrix pointMat = Point3D.pointArrayToMatrix(new Point3D[]{point}, true);
        invertedPointMat = prevSrcTransform.applyInverseToPoints(pointMat);
    }

    double x = invertedPointMat.get(0,0);
    double y = invertedPointMat.get(0,1);
    double z = invertedPointMat.get(0,2);

    return new Point3D(new double[]{x,y,z});

}

private void checkSelectedWindows(){
    //checks that each selected window corresponds to an open window
    //if any windows are missing, unselect them
    WindowListModel[] windowsToCheck = {
        windowListSrcPts,
        windowListDestPts,
        windowListSrcTransformPts,
        windowListSrcTransform,
        windowListDestTransform,
    };
    for(int i = 0; i < windowsToCheck.length; i++){
        if(WindowManager.getImage(windowsToCheck[i].getSelectedID()) == null){
            //unselect this item
            windowsToCheck[i].setSelectedID(0);
            windowsToCheck[i].setSelectedItem(null);
        }
    }
}

private Point3D transformPoint(Point3D point){

    //uses prevSrcTransform to transform an arraylist of points
    Matrix transformedPointMat;
    if(prevSrcTransform.getClass().equals(AffineTransform2D.class) ||
            prevSrcTransform.getClass().equals(PiecewiseAffineTransform2D.class)){
        //the 2D transform can be a little weird. First we need a 2D point matrix:
        Matrix pointMat = Point3D.pointArrayToMatrix2D(new Point3D[]{point}, true);
        transformedPointMat = prevSrcTransform.applyToPoints(pointMat);

        //But now we've lost any stack information we had in the Z-coordinate. Recover that.
        transformedPointMat.set(0, 2, point.z);
    }
    else{
        //the 3D transform is easy
        Matrix pointMat = Point3D.pointArrayToMatrix(new Point3D[]{point}, true);
        transformedPointMat = prevSrcTransform.applyToPoints(pointMat);
    }
    double x = transformedPointMat.get(0,0);
    double y = transformedPointMat.get(0,1);
    double z = transformedPointMat.get(0,2);

    return new Point3D(new double[]{x,y,z});
}

private PointRoi point3DToRoi(Point3D point, int imageID){
    PointRoi roi = new PointRoi(point.x, point.y);
    ImagePlus imp = WindowManager.getImage(imageID);
    roi.setImage(imp);

    //an ROI's position is affected by how many channels there are. Need to
    //multiply by nChannels to put it in the right place.
    int zpos = (int) (point.z * imp.getNChannels());
    print(">" + imp.getTitle());
    print("> Setting ROI Z-position to " + zpos + " from point " + point.x + "," + point.y + "," + point.z);
    roi.setPosition(zpos);
    print("> ROI zpos set to " + roi.getPosition());

    return roi;
}

private Point3D roiToPoint3D(PointRoi ptRoi){
    //assumes ptRoi has only one point in it!
    //will only pull first point out of multipoint rois.
    double x = ptRoi.getPolygon().xpoints[0];
    double y = ptRoi.getPolygon().ypoints[0];

    //an ROI's position is affected by how many channels there are. Need to divide
    //by that and floor it to get the actual position.
    int numChannels = WindowManager.getImage(ptRoi.getImageID()).getNChannels();
    double z = ptRoi.getPosition() / numChannels;

    print("< ptRoi position is " + ptRoi.getPosition() + " nChannels " + numChannels + "  ");
    print("< Setting point3D z to " + z);
    Point3D p = new Point3D(new double[]{x,y,z});
    return p;
}

private void updateTablesToRois(){
    //make sure all selected windows are actually open before we do this
    checkSelectedWindows();

    getManagerInstance();

    Manager.getList().removeAll();

    print("@ Changed to window: " + windowListDestPts.getSelectedID());
    IJ.selectWindow(windowListDestPts.getSelectedID());

    for(int i = 0; i < srcPtsTable.points.size(); i++){
        PointRoi pointRoi = point3DToRoi(srcPtsTable.points.get(i), windowListSrcPts.getSelectedID());
        print("@ sending source point " + srcPtsTable.points.get(i).toString() + " to roi " + pointRoi.toString() + pointRoi.getPosition());
        Manager.addRoi(pointRoi);
        Roi[] rois = Manager.getRoisAsArray();
        PointRoi mgrRoi = (PointRoi) rois[rois.length - 1];
        print("@ src roi added: " + mgrRoi.toString() + mgrRoi.getPosition());
    }
    for(int i = 0; i < destPtsTable.points.size(); i++){
        PointRoi pointRoi = point3DToRoi(destPtsTable.points.get(i), windowListDestPts.getSelectedID());
        print("@ sending dest point " + destPtsTable.points.get(i).toString() + " to roi " + pointRoi.toString() + pointRoi.getPosition());
        Manager.addRoi(pointRoi);
        Roi[] rois = Manager.getRoisAsArray();
        PointRoi mgrRoi = (PointRoi) rois[rois.length - 1];
        print("@ dest roi added: " + mgrRoi.toString() + mgrRoi.getPosition());
    }


    //Debug: Pull all the ROIs out of the manager and interrogate their Z-positions
    Roi[] rois = Manager.getRoisAsArray();

    for(int roiIndex = 0; roiIndex < rois.length; roiIndex++) {
        Roi roi = rois[roiIndex];
        if(roi.getClass().equals(PointRoi.class)){
            PointRoi ptRoi = (PointRoi) roi;
            if(ptRoi.getImageID() == windowListSrcPts.getSelectedID()){
                print("@ source point:");
            }
            else if(ptRoi.getImageID() == windowListDestPts.getSelectedID()){
                print("@ dest point:");
            }
            else{
                print("@ ??? point:");
            }
            print("@ " + ptRoi.toString() + ", " + ptRoi.getPosition() + " " + ptRoi.getImageID());
        }
    }
    //update roi names and calculate srcTransformed points, if needed
    updateRoisToTables();
}

private void getManagerInstance(){
    if (Manager.getInstance() == null)
        Manager = new RoiManager();
    else
        Manager = Manager.getInstance();
}

private void updateRoisToTables(){
    //make sure all selected windows are actually open before we do this
    checkSelectedWindows();

    srcPtsTable.clearPoints();
    destPtsTable.clearPoints();

    //go through all ROIs
    //if the ROI is associated with any of our images,
    //rename it to something sane
    //if it's a multipoint ROI, expand it to several single-point ROIs.
    //Refresh JTables.

    getManagerInstance();

    Roi[] rois = Manager.getRoisAsArray();

    ArrayList<PointRoi> sourceRois = new ArrayList<PointRoi>();
    ArrayList<PointRoi> destinationRois = new ArrayList<PointRoi>();
    ArrayList<PointRoi> sourceTransformedRois = new ArrayList<PointRoi>();

    //also maintain any point ROIs not currently associated with an image
    //the user may not have defined the source and dest windows yet!
    ArrayList<PointRoi> otherRois = new ArrayList<PointRoi>();

    //clear the ROI manager out and redraw all the ROIs as single-point ROIs
    //with intuitive names

    for(int roiIndex = 0; roiIndex < rois.length; roiIndex++){
        //where did this ROI come from?
        Roi roi = rois[roiIndex];
        int imageID = roi.getImageID();

        //we need to split up any multi-point ROIs into being individual ones.
        if(roi.getClass().equals(PointRoi.class)){
            //it's a point ROI! We want it.
            //Make a new point ROI for each
            //point in it.

            PointRoi ptRoi = (PointRoi) roi;
            for(int i = 0; i < ptRoi.getPolygon().npoints; i++){
                PointRoi singlePointRoi = new PointRoi(ptRoi.getPolygon().xpoints[i], ptRoi.getPolygon().ypoints[i]);
                ImagePlus imp = WindowManager.getImage(ptRoi.getImageID());
                print("+ image: " + imp.getTitle() + " channels " + imp.getNChannels() + " roi " + ptRoi.toString() + ptRoi.getPosition());
                if(imp == null){
                    continue;
                }
                singlePointRoi.setImage(imp);

                if(imp.getNChannels() == 1){
                    singlePointRoi.setPosition(ptRoi.getPosition());
                }
                else{
                    singlePointRoi.setPosition(ptRoi.getPosition() * imp.getNChannels());
                }

                if(imageID == windowListSrcPts.getSelectedID()){
                    //it's in the source image
                    print("+ adding " + roiIndex + " to src: " + ptRoi.toString() + ptRoi.getPosition());
                    sourceRois.add(singlePointRoi);
                }
                else if(imageID == windowListDestPts.getSelectedID()){
                    //it's in the dest image
                    print("+ adding " + roiIndex + " to dest: " + ptRoi.toString() + ptRoi.getPosition());
                    destinationRois.add(singlePointRoi);
                }
                else if(imageID == windowListSrcTransformPts.getSelectedID()){
                    //it's in srcTransformed
                    print("+ adding " + roiIndex + " to srcTransformed: " + ptRoi.toString() + ptRoi.getPosition());
                    sourceTransformedRois.add(singlePointRoi);
                }
                else{
                    print("+ adding " + roiIndex + " to other: " + ptRoi.toString() + ptRoi.getPosition());
                    otherRois.add(singlePointRoi);
                }
            }
        }
    }

    //The ROIs in sourceRois and the ROIs in sourceTransformedRois
    //need to match up. If there are more ROIs in one, calculate the other.
    if(prevSrcTransform != null && windowListSrcTransformPts.getSelectedID() < 0){
        if(sourceRois.size() > sourceTransformedRois.size()){
            //make the srcTransformedRoi for this sourceRoi
            for(int i = sourceTransformedRois.size(); i < sourceRois.size(); i++){
                Point3D sourceRoiPoint = roiToPoint3D(sourceRois.get(i));
                Point3D sourceTransformedRoiPoint = transformPoint(sourceRoiPoint);
                PointRoi pointRoi = point3DToRoi(sourceTransformedRoiPoint, windowListSrcTransformPts.getSelectedID());
                sourceTransformedRois.add(pointRoi);
            }
        }
        if(sourceTransformedRois.size() > sourceRois.size()){
            //make the sourceRoi for this srcTransformedRoi
            for(int i = sourceRois.size(); i < sourceTransformedRois.size(); i++){
                Point3D sourceTransformedRoiPoint = roiToPoint3D(sourceTransformedRois.get(i));
                Point3D sourceRoiPoint = invertPoint(sourceTransformedRoiPoint);
                PointRoi pointRoi = point3DToRoi(sourceRoiPoint, windowListSrcPts.getSelectedID());
                sourceRois.add(pointRoi);
            }
        }
    }

    //now we have three sets of single-point ROIs.
    //Add them back into the ROI manager, and name them nicely.
    //Also add them to the relevant JTable.

    int maxIndex = 0;
    if(sourceRois.size() > maxIndex)
        maxIndex = sourceRois.size();
    if(destinationRois.size() > maxIndex)
        maxIndex = destinationRois.size();
    if(sourceTransformedRois.size() > maxIndex)
        maxIndex = sourceTransformedRois.size();

    //clear out old ones first
    Manager.runCommand("Reset");
    print("+ Changed to window: " + windowListDestPts.getSelectedID());
    IJ.selectWindow(windowListDestPts.getSelectedID());

    for(int i = 0; i < maxIndex; i++){
        if(i < sourceRois.size()){
            sourceRois.get(i).setName("Source " + (i+1));
            Manager.addRoi(sourceRois.get(i));
            srcPtsTable.addPoint(roiToPoint3D(sourceRois.get(i)));
            print("+ Added src roi: " + sourceRois.get(i).toString());
        }
        if(i < sourceTransformedRois.size()){
            sourceTransformedRois.get(i).setName("Transformed " + (i+1));
            Manager.addRoi(sourceTransformedRois.get(i));
            print("+ Added src trans roi: " + sourceTransformedRois.get(i).toString());
        }
        if(i < destinationRois.size()){
            destinationRois.get(i).setName("Destination " + (i+1));
            Manager.addRoi(destinationRois.get(i));
            destPtsTable.addPoint(roiToPoint3D(destinationRois.get(i)));
            print("+ Added dest roi: " + destinationRois.get(i).toString());
        }
    }
    for(int i = 0; i < otherRois.size(); i++){
        print("+ Adding 'other' roi:" + otherRois.get(i).toString());
        Manager.addRoi(otherRois.get(i));
    }

}

private void btnRefreshPointsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshPointsActionPerformed
    refreshWindowList();
    updateRoisToTables();
    updateTransformInfo();
}//GEN-LAST:event_btnRefreshPointsActionPerformed

private void refreshWindowList(){
    int[] windowIDs = WindowManager.getIDList();
    if(windowIDs == null){
        return;
    }
    WindowListItem[] items = new WindowListItem[windowIDs.length];
    for(int i = 0; i < windowIDs.length; i++){
        items[i] = new WindowListItem();
        items[i].windowID = windowIDs[i];

        ImagePlus imp = WindowManager.getImage(windowIDs[i]);
        if (imp != null)
            items[i].windowTitle = imp.getTitle();
        else
            items[i].windowTitle = "";
    }

    WindowListModel[] windowLists = {
        windowListSrcPts,
        windowListDestPts,
        windowListSrcTransformPts,
        windowListSrcTransform,
        windowListDestTransform
    };

    for(int i = 0; i < windowLists.length; i++){
        windowLists[i].setWindowList(items);
    }
}

private void btnRefreshWindowListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshWindowListActionPerformed
    refreshWindowList();
}//GEN-LAST:event_btnRefreshWindowListActionPerformed

private void btnTransformActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTransformActionPerformed

    //get points
    ArrayList<Point3D> srcPtsArrayList = srcPtsTable.getPoints();
    ArrayList<Point3D> destPtsArrayList = destPtsTable.getPoints();

    if(srcPtsArrayList.size() != destPtsArrayList.size()){
        IJ.showMessage("Error: source points and dest points are of different length.");
    }

    if(srcPtsArrayList.size() < 4){
        IJ.showMessage("Error: Need at least 4 points to perform transforms.");
    }

    //jiggle points to remove any coplanarity
    //crude, but effective
    for(int i = 0; i < srcPtsArrayList.size(); i++){

        srcPtsArrayList.get(i).x = srcPtsArrayList.get(i).x + Math.random() * Constants.EPSILON - Constants.EPSILON/2;
        srcPtsArrayList.get(i).y = srcPtsArrayList.get(i).y + Math.random() * Constants.EPSILON - Constants.EPSILON/2;
        srcPtsArrayList.get(i).z = srcPtsArrayList.get(i).z + Math.random() * Constants.EPSILON - Constants.EPSILON/2;

        destPtsArrayList.get(i).x = destPtsArrayList.get(i).x + Math.random() * Constants.EPSILON - Constants.EPSILON/2;
        destPtsArrayList.get(i).y = destPtsArrayList.get(i).y + Math.random() * Constants.EPSILON - Constants.EPSILON/2;
        destPtsArrayList.get(i).z = destPtsArrayList.get(i).z + Math.random() * Constants.EPSILON - Constants.EPSILON/2;
    }

    //get images / imageStacks
    ImagePlus impSrc = WindowManager.getImage(windowListSrcTransform.getSelectedItem().windowTitle);
    ImagePlus impDest = WindowManager.getImage(windowListDestTransform.getSelectedItem().windowTitle);

    //verify 8bit-ness
    int srcDepth = impSrc.getBitDepth();
    int destDepth = impDest.getBitDepth();
    if(srcDepth > 8 || destDepth > 8){
        IJ.showMessage("Error: source and dest stacks must be 8-bit to run transform.");
        return;
    }

    String outDir = txtTransformedOutDir.getText();
    if(! outDir.endsWith("/"))
        outDir += "/";


    Matrix[] ptMats = getPointsAsMatrices();
    Matrix srcPtsMat = ptMats[0];
    Matrix destPtsMat = ptMats[1];
    boolean is2D = MatrixMethods.isTransform2D(srcPtsArrayList, destPtsArrayList);

    Transform thisTransform = null;

    try{
        //determine transform type
        if(radioLSAffine.isSelected()){
            //least squares affine transform
            if(is2D){
                thisTransform = new AffineTransform2D(srcPtsMat, destPtsMat);
            }
            else{
                thisTransform = new AffineTransform3D(srcPtsMat, destPtsMat);
            }
        }
        else{
            //do piecewise
            if(is2D){
                //nothing here yet! Need a 2D delaunay. Go hunt for one or make one later.
            }
            else{
                //3D piecewise.

                //We add corner points based on an affine transform to provide
                //a reasonably sane extrapolation function.

                ArrayList<Point3D> srcWithCorners = new ArrayList<Point3D>();
                srcWithCorners.addAll(srcPtsArrayList);

                ArrayList<Point3D> destWithCorners = new ArrayList<Point3D>();
                destWithCorners.addAll(destPtsArrayList);

                //calculate corners
                int sw = impSrc.getStack().getWidth();
                int sh = impSrc.getStack().getHeight();
                int sd = impSrc.getStack().getSize();
                double[][] srcCornersDouble = {
                    {0,0,0,1},
                    {sw,0,0,1},
                    {0,sh,0,1},
                    {sw,sh,0,1},
                    {0,0,sd,1},
                    {sw,0,sd,1},
                    {0,sh,sd,1},
                    {sw,sh,sd,1}
                };

                for(int i = 0; i < srcCornersDouble.length; i++){
                    for(int j = 0; j < 3; j++){
                        srcCornersDouble[i][j] += Math.random() * Constants.EPSILON - Constants.EPSILON/2;
                    }
                }

                Matrix srcCornersMat = new Matrix(srcCornersDouble);
                AffineTransform3D aff = new AffineTransform3D(srcPtsMat, destPtsMat);
                Matrix destCornersMat = aff.applyToPoints(srcCornersMat);

                srcWithCorners.addAll(new ArrayList<Point3D>(Arrays.asList(Point3D.matrixToPointArray(srcCornersMat))));
                destWithCorners.addAll(new ArrayList<Point3D>(Arrays.asList(Point3D.matrixToPointArray(destCornersMat))));

                int dw = impDest.getStack().getWidth();
                int dh = impDest.getStack().getHeight();
                int dd = impDest.getStack().getSize();
                Point3D[] destImageCorners = new Point3D[]{
                    new Point3D(new double[]{0,0,0}),
                    new Point3D(new double[]{dw,0,0}),
                    new Point3D(new double[]{0,dh,0}),
                    new Point3D(new double[]{0,0,dd}),
                    new Point3D(new double[]{0,dh,dd}),
                    new Point3D(new double[]{dw,0,dd}),
                    new Point3D(new double[]{dw,dh,0}),
                    new Point3D(new double[]{dw,dh,dd}),
                };

                Box3D destImageBox = new Box3D(destImageCorners);

                //make transform
                //yes, it does go dest and then src, it's weird like that
                //so we build a tessellation that is based on the destination points
                //and then when we need to determine pixel values for srcTransformed
                //we can easily look up which tetrahedron contains the srcTransformed
                //point.
                thisTransform = new PiecewiseAffineTransform3D(destWithCorners,srcWithCorners,destImageBox);
            }
        }

        ImagePlus[] results = thisTransform.applyToImage(impSrc, impDest, radioCropToDest.isSelected());

        //save srcTransformed
        if(chkWriteOutImages.isSelected()){
            ImageStack transformedStack = results[0].getImageStack();
            for(int i = 0; i < transformedStack.getSize(); i++){
                ImageProcessor ip = transformedStack.getProcessor(i+1);
                ImagePlus imp = new ImagePlus("Transformed Volume", ip);
                FileSaver fs = new FileSaver(imp);
                fs.saveAsTiff(outDir + String.format("%05d", i) + ".tif");
            }
        }

        //open srcTransformed
        results[0].show();

        prevSrcTransform = thisTransform;

        //update srcTransformed in window list
        refreshWindowList();
        windowListSrcTransformPts.setSelectedID(results[0].getID());
        //transform ROIs from source and copy them to srcTransformed
        updateRoisToTables();

        //open destTranslated if needed
        if(! radioCropToDest.isSelected()){
            //need to translate the dest image
            //so that srcTransformed fits on top of it
        }
    }
    catch(Exception ex){
        ex.printStackTrace();
    }
    print("Transform complete!");
}//GEN-LAST:event_btnTransformActionPerformed

private Matrix[] getPointsAsMatrices(){
    Matrix srcPtsMat = null;
    Matrix destPtsMat = null;

    //check if we're in 2D or 3D
    //if the Z-coordinates are planar in each point set, it's 2D
    boolean is2D = MatrixMethods.isTransform2D(srcPtsTable.points, destPtsTable.points);
    if(is2D){
        srcPtsMat = Point3D.pointArrayToMatrix2D(srcPtsTable.points.toArray(new Point3D[]{}), true);
        destPtsMat = Point3D.pointArrayToMatrix2D(destPtsTable.points.toArray(new Point3D[]{}), true);
    }
    else{
        srcPtsMat = Point3D.pointArrayToMatrix(srcPtsTable.points.toArray(new Point3D[]{}), true);
        destPtsMat = Point3D.pointArrayToMatrix(destPtsTable.points.toArray(new Point3D[]{}), true);
    }

    return new Matrix[]{srcPtsMat, destPtsMat};
}

private void updateTransformInfo(){
    int numPointPairs = Math.min(srcPtsTable.points.size(),destPtsTable.points.size());
    if(numPointPairs < 4){
        txtTransformInfo.setText("Transform info will be calculated here when at least four 3D point pairs are selected.\n");
        return;
    }
    boolean is2D = MatrixMethods.isTransform2D(srcPtsTable.points, destPtsTable.points);
    if(is2D){
        txtTransformInfo.setText("Transform info will be calculated here when at least four 3D point pairs are selected.\n");
        return;
    }



    Matrix[] ptMats = getPointsAsMatrices();
    Matrix srcPtsMat = ptMats[0];
    Matrix destPtsMat = ptMats[1];

    AffineTransform3D aff = new AffineTransform3D(srcPtsMat, destPtsMat);

    StringBuilder s = new StringBuilder();

    //find angle of transform
    s.append("Z axis is rotated by " + Math.round(aff.getZRotationAngle()*100.0)/100.0 + " degrees.\n\n");

    if(numPointPairs >= 5){
        //calculate worst outlier
        String err = AffineInfo.getWorstPointPair(srcPtsMat, destPtsMat, aff);
        s.append(err);
    }

    txtTransformInfo.setText(s.toString());
}

private void savePoints(String filePath){
    StringBuilder s = new StringBuilder("");
    s.append("Source Points:\r\n");
    for(int i = 0; i < srcPtsTable.points.size(); i++){
        s.append(srcPtsTable.points.get(i).toString());
        s.append("\r\n");
    }
    s.append("\r\n");

    s.append("Destination Points:\r\n");
    for(int i = 0; i < destPtsTable.points.size(); i++){
        s.append(destPtsTable.points.get(i).toString());
        s.append("\r\n");
    }
    s.append("\r\n");

    FileAndDirOperations.writeStringToFile(s.toString(), filePath);

}

private void loadPoints(String filePath){
    //clear points tables
    srcPtsTable.clearPoints();
    destPtsTable.clearPoints();


    //read source and dest points from file
    String pointsStr = FileAndDirOperations.readFileIntoString(filePath);
    String[] lines = pointsStr.split("\\n");

    boolean readingSourcePoints = false;
    boolean readingDestinationPoints = false;

    for(int i = 0; i < lines.length; i++){
        String line = lines[i];
        if(line.toLowerCase().contains("source")){
            readingSourcePoints = true;
            readingDestinationPoints = false;
        }
        else if(line.toLowerCase().contains("destination")){
            readingSourcePoints = false;
            readingDestinationPoints = true;
        }
        else if(!line.trim().isEmpty()){
            //parse the point on this line
            try{
                line = line.trim().replaceAll("\\s+", ",");
                String[] numbers = line.split(",");
                double x = Double.parseDouble(numbers[0]);
                double y = Double.parseDouble(numbers[1]);
                double z = Double.parseDouble(numbers[2]);
                Point3D p = new Point3D(new double[]{x,y,z});

                if(readingSourcePoints){
                    srcPtsTable.addPoint(p);
                }
                else if(readingDestinationPoints){
                    destPtsTable.addPoint(p);
                }
            }
            catch(Exception ex){
                System.out.println("Couldn't read file " + filePath + " on line " + i+1);
                ex.printStackTrace();
            }
        }
    }
    updateTablesToRois();
}

private void btnSavePointsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSavePointsActionPerformed
    JFileChooser chooser = new JFileChooser();
    chooser.setCurrentDirectory(new java.io.File("."));
    chooser.setDialogTitle("Save Points As...");
    chooser.setSelectedFile(new File("points.txt"));
    int returnVal = chooser.showSaveDialog(null);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        String filePath = chooser.getSelectedFile().getAbsolutePath();
        savePoints(filePath);
    }
}//GEN-LAST:event_btnSavePointsActionPerformed

private void btnLoadPointsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadPointsActionPerformed
    JFileChooser chooser = new JFileChooser();
    chooser.setCurrentDirectory(new java.io.File("."));
    chooser.setDialogTitle("Load Points From...");
    int returnVal = chooser.showOpenDialog(null);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        String filePath = chooser.getSelectedFile().getAbsolutePath();
        loadPoints(filePath);
    }
    updateTransformInfo();

}//GEN-LAST:event_btnLoadPointsActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLoadPoints;
    private javax.swing.JButton btnRefreshPoints;
    private javax.swing.JButton btnRefreshWindowList;
    private javax.swing.JButton btnSavePoints;
    private javax.swing.JButton btnTransform;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JCheckBox chkWriteOutImages;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JComboBox jComboBox4;
    private javax.swing.JComboBox jComboBox5;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JRadioButton radioCropToDest;
    private javax.swing.JRadioButton radioLSAffine;
    private javax.swing.JRadioButton radioTranslateDest;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JTextArea txtHelp;
    private javax.swing.JTextArea txtTransformInfo;
    private javax.swing.JTextField txtTransformedOutDir;
    // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;

    private JDialog aboutBox;


    public static void print(String s){
        System.out.println(s);
        //IJ.log(s); //Debug mode only!
    }
}
