
package transforms;

import Jama.Matrix;
import transforms.AffineTransform3D;
import transforms.PiecewiseAffineTransform3D;
import geometry.Point3D;
import geometry.Point3D;
import ij.ImageStack;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import java.awt.geom.AffineTransform;

public class PiecewiseAffineImageTransformer3D {
      
    private AffineTransform3D affineTransform; //holds the best-fit affine
    private PiecewiseAffineTransform3D del; //holds the piecewise affine transform
    private PiecewiseAffineTransform3D[] delCopies; //holds copies of del used by each thread

    //inputs
    private ImageStack srcStack;
    private ImageStack destStack;
    
    //outputs
    private ImageStack srcTransformedStack;
    private ImageStack destTranslatedStack;
    
    //all the rest are calculated from inputs
    private double sw;
    private double sh;
    private double sd;
    
    private int outImageHeight;
    private int outImageWidth;
    private int outImageDepth;
    
    private int destTranslateX;
    private int destTranslateY;
    private int destTranslateZ;
    
    private boolean cropToDest;
    
    public PiecewiseAffineImageTransformer3D(AffineTransform3D affineTransform_, PiecewiseAffineTransform3D del_, ImageStack srcStack_, ImageStack destStack_, boolean cropToDest_){
        affineTransform = affineTransform_;
        del = del_;
        srcStack = srcStack_;
        destStack = destStack_;
        cropToDest = cropToDest_;
        
        sw = srcStack.getWidth();
        sh = srcStack.getHeight();
        sd = srcStack.getSize();

        //determine size of output image. 
        //Needs to at least hold destImage.
        //This assumes we're not cropping to dest;
        //the cropped case is handled further down.
        double outMinX = 0;
        double outMinY = 0;
        double outMinZ = 0;
        double outMaxX = destStack.getWidth();
        double outMaxY = destStack.getHeight();
        double outMaxZ = destStack.getSize();
        
        //apply forward transform to the src image corners
        //if it goes outside the bounds of destImage, extend outImage in that direction
        double[][] srcCornersDouble = {
            {0,0,0,1},
            {sw,0,0,1},
            {0,sh,0,1},
            {sw,sh,0,1},
            {0,0,sd,1},
            {sw,0,sd,1},
            {0,sh,sd,1},
            {sw,sh,sd,1}
        };
        Matrix srcCorners = new Matrix(srcCornersDouble);
        Matrix srcCornersAffine = affineTransform.applyToPoints(srcCorners);
        for(int i = 0; i < srcCorners.getRowDimension(); i++){
            print("corner x: " + srcCornersAffine.get(i,0) + " y: " + srcCornersAffine.get(i,1) + " z: " +  srcCornersAffine.get(i,2));
            if(srcCornersAffine.get(i,0) < outMinX){
                outMinX = srcCornersAffine.get(i,0);
            } 
            if(srcCornersAffine.get(i,0) > outMaxX){
                outMaxX = srcCornersAffine.get(i,0);
            } 
            if(srcCornersAffine.get(i,1) < outMinY){
                outMinY = srcCornersAffine.get(i,1);
            } 
            if(srcCornersAffine.get(i,1) > outMaxY){
                outMaxY = srcCornersAffine.get(i,1);
            } 
            if(srcCornersAffine.get(i,2) < outMinZ){
                outMinZ = srcCornersAffine.get(i,2);
            } 
            if(srcCornersAffine.get(i,2) > outMaxZ){
                outMaxZ = srcCornersAffine.get(i,2);
            } 
        }
        
        outImageWidth = (int) Math.ceil(outMaxX - outMinX);
        outImageHeight = (int) Math.ceil(outMaxY - outMinY);
        outImageDepth = (int) Math.ceil(outMaxZ - outMinZ);   
        
        //dest image will move iff srcTransform goes below 0.
        destTranslateX = (int) Math.round(0-outMinX);
        destTranslateY = (int) Math.round(0-outMinY);
        destTranslateZ = (int) Math.round(0-outMinZ);
        
        //BUT! All this assumes we're not cropping the output image.
        //If we are, it's much easier:
        if(cropToDest){
            //width height and depth are defined solely by the dest stack:
            outImageWidth = destStack.getWidth();
            outImageHeight = destStack.getHeight();
            outImageDepth = destStack.getSize();
        }
        
        destTranslatedStack = new ImageStack(outImageWidth,outImageHeight);
        srcTransformedStack = new ImageStack(outImageWidth,outImageHeight);
        for(int i = 0; i < outImageDepth; i++){
            ImageProcessor srcTransformed = new ByteProcessor(outImageWidth,outImageHeight);
            srcTransformedStack.addSlice(""+i, srcTransformed);
            ImageProcessor destTranslated = new ByteProcessor(outImageWidth,outImageHeight);
            destTranslatedStack.addSlice(""+i, destTranslated);
        }
        
    }
    
    //thread sync crap
    //don't touch these variables other than setting them to 0 before starting
    //only increment them using the functions below.
    private int nextDepthNeeded = 0; //set this to 0 before use; don't touch otherwise
    private synchronized int getNextDepthNeeded(){
        nextDepthNeeded++;
        return nextDepthNeeded;
    }
    
    private int threadsCompleted = 0;
    private synchronized void incrementThreadsCompleted(){
        threadsCompleted++;
    }
    
    
    public ImageStack[] doTransform(int numThreads){
        //fill in destTranslated output
        //no need to multithread this, it's really quick
        for(int d = 0; d < destStack.getSize(); d++){
            for(int i = 0; i < destStack.getHeight(); i++){
                for(int j = 0; j < destStack.getWidth(); j++){
                    int pixelValue = (int) destStack.getVoxel(j,i,d);
                    if(cropToDest){
                        destTranslatedStack.setVoxel(j, i, d, pixelValue);
                    }
                    else{
                        destTranslatedStack.setVoxel(j+destTranslateX, i+destTranslateY, d+destTranslateZ, pixelValue);
                    }
                }
            }
        }
        
        
        //just temp debug stuff
        print("\nDelaunay points:");
        for(int i = 0; i < del.points.size(); i++){
            print("" + del.points.get(i));
        }
        
        //make copies of del for each thread
        delCopies = new PiecewiseAffineTransform3D[numThreads];
        for(int i = 0; i < numThreads; i++){
            print("copying tessellation (" + i + " of " + numThreads + ")");
            delCopies[i] = del.copy();
        }
        
        //now fill in the srcTransformedStack
        //this is where the threads come in, so
        //make a bunch of threads and exec them
        resetThreadNum();
        for(int i = 0; i < numThreads; i++){
            ImageTransformThread t = new ImageTransformThread();
            t.start();
        }
        
        //wait for threads to finish
        threadsCompleted = 0;
        while(threadsCompleted < numThreads){
            try{
                Thread.sleep(1000);            
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
        
        return new ImageStack[] {srcTransformedStack,destTranslatedStack};
    }
    
    private void print(String s){
        System.out.println(s);
    }

    private int threadCounter = -1;
    private synchronized int getThreadNum(){
        threadCounter++;
        return threadCounter;
    }
    private void resetThreadNum(){
        threadCounter = -1;
    }
    
    //these threads actually do all the work
    public class ImageTransformThread extends Thread{
        //run from applyToImage function
        
        //srcStack needs to be read accessible
        //srcTransformedStack needs to be write accessible
        
        public void run(){
            //basically you want to parallelize this loop.
            //have a synced object from which threads claim different
            //depths {d=0:n}.
            
            int threadNum = getThreadNum();
            
            while(true){
                int d = getNextDepthNeeded();
                if(d > outImageDepth){
                    incrementThreadsCompleted();
                    return;
                }
                if(d % 10 == 0){
                    print("" + (new Double(d*100)/outImageDepth) + "% done.");
                }
                for(int i = 0; i < outImageHeight; i++){
                    for(int j = 0; j < outImageWidth; j++){
                        //We need to find the input image pixels
                        //needed to produce the output pixel at this spot.
                        Matrix inputSpot;
                        if(cropToDest){
                            double[][] outPixelSpot = {{j,i,d,1}};
                            Matrix outPixelSpotMat = new Matrix(outPixelSpot);
                            AffineTransform3D tform = delCopies[threadNum].getTransform(new Point3D(new double[]{j,i,d}));
                            if(tform == null){
                                inputSpot = outPixelSpotMat.times(affineTransform.getInverseTransform());
                            }
                            else{
                                inputSpot = outPixelSpotMat.times(tform.getTransform());
                            }
                            
                        }
                        else{
                            //no cropping
                            double[][] outPixelSpot = {{j-destTranslateX,i-destTranslateY,d-destTranslateZ,1}};
                            Matrix outPixelSpotMat = new Matrix(outPixelSpot);
                            AffineTransform3D tform = delCopies[threadNum].getTransform(new Point3D(new double[]{j,i,d}));
                            if(tform == null){
                                inputSpot = outPixelSpotMat.times(affineTransform.getInverseTransform());
                            }
                            else{
                                inputSpot = outPixelSpotMat.times(tform.getTransform());
                            }

                        }
                        
                        double x = inputSpot.get(0,0);
                        double y = inputSpot.get(0,1);
                        double z = inputSpot.get(0,2);

                        int xLow = (int) Math.floor(x);
                        int xHigh = xLow+1;
                        int yLow = (int) Math.floor(y);
                        int yHigh = yLow+1;
                        int zLow = (int) Math.floor(z);
                        int zHigh = zLow+1;


                        //bounds check - make sure we're in the source image
                        if(xHigh < sw && xLow > 0 && yHigh < sh && yLow > 0 && zHigh < sd && zLow > 0 ){
                            //interpolate nearby pixels
                            double leftUpLow = srcStack.getVoxel(xLow,yLow,zLow);
                            double rightUpLow = srcStack.getVoxel(xHigh,yLow,zLow);
                            double leftDownLow = srcStack.getVoxel(xLow,yHigh,zLow);
                            double rightDownLow = srcStack.getVoxel(xHigh,yHigh,zLow);

                            double leftUpHigh = srcStack.getVoxel(xLow,yLow,zHigh);
                            double rightUpHigh = srcStack.getVoxel(xHigh,yLow,zHigh);
                            double leftDownHigh = srcStack.getVoxel(xLow,yHigh,zHigh);
                            double rightDownHigh = srcStack.getVoxel(xHigh,yHigh,zHigh);

                            double xOffset = x-xLow;
                            double yOffset = y-yLow;
                            double zOffset = z-zLow;

                            double xAvg1 = leftUpLow + xOffset*(rightUpLow-leftUpLow);
                            double xAvg2 = leftDownLow + xOffset*(rightDownLow-leftDownLow);
                            double xAvg3 = leftUpHigh + xOffset*(rightUpHigh-leftUpHigh);
                            double xAvg4 = leftDownHigh + xOffset*(rightDownHigh-leftDownHigh);

                            double yAvg1 = xAvg1 + yOffset*(xAvg2-xAvg1);
                            double yAvg2 = xAvg3 + yOffset*(xAvg4-xAvg3);

                            int pixelValue = (int) Math.round(yAvg1 + zOffset*(yAvg2-yAvg1));
                            srcTransformedStack.setVoxel(j, i, d, pixelValue);
                        }
                        else{
                            //not in source image, so output pixel can stay black
                            continue;
                        }


                    }        
                }
            }
        }
    }
}
