/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import Jama.Matrix;
import pointpatternmatcher_.Constants;

public class Sphere {
    public double radius = 0;
    public Point3D center = new Point3D();
    
    public Sphere(double radius_, Point3D center_){
        //basic constructor
        radius = radius_;
        center = center_;
    }
    
    public Sphere(Point3D[] p){
        //calculates a sphere tangent to four points
        //uses some determinant code found on teh internets, looked legit
        
        //If points are coplanar, return a radius=0.
        
        int i;
        
        /* Find determinant M11 */    
        double[][] a = new double[4][4];
        for (i=0;i<4;i++) {
          a[i][0] = p[i].x;
          a[i][1] = p[i].y;
          a[i][2] = p[i].z;
          a[i][3] = 1;
        }
        Matrix matA = new Matrix(a);
        double m11 = matA.det();

        /* Find determinant M12 */    
        for (i=0;i<4;i++) {
          a[i][0] = p[i].x*p[i].x + p[i].y*p[i].y + p[i].z*p[i].z;
          a[i][1] = p[i].y;
          a[i][2] = p[i].z;
          a[i][3] = 1;
        }
        matA = new Matrix(a);
        double m12 = matA.det();

        /* Find determinant M13 */    
        for (i=0;i<4;i++) {
          a[i][0] = p[i].x;
          a[i][1] = p[i].x*p[i].x + p[i].y*p[i].y + p[i].z*p[i].z;
          a[i][2] = p[i].z;
          a[i][3] = 1;
        }
        matA = new Matrix(a);
        double m13 = matA.det();

        /* Find determinant M14 */    
        for (i=0;i<4;i++) {
          a[i][0] = p[i].x;
          a[i][1] = p[i].y;
          a[i][2] = p[i].x*p[i].x + p[i].y*p[i].y + p[i].z*p[i].z;
          a[i][3] = 1;
        }
        matA = new Matrix(a);
        double m14 = matA.det();

        /* Find determinant M15 */    
        for (i=0;i<4;i++) {
          a[i][0] = p[i].x*p[i].x + p[i].y*p[i].y + p[i].z*p[i].z;
          a[i][1] = p[i].x;
          a[i][2] = p[i].y;
          a[i][3] = p[i].z;
        }
        matA = new Matrix(a);
        double m15 = matA.det();

        if (Math.abs(m11) < Constants.EPSILON) {
            //tetrahedron is degenerate (near-planar).
            radius = 0;            
            //System.out.println("The points don't define a sphere! Points are colinear or coplanar.");
        }
        else{
            center.x = 0.5 * m12 / m11;
            center.y = 0.5 * m13 / m11;
            center.z = 0.5 * m14 / m11;
            radius = Math.sqrt(center.x*center.x + center.y*center.y + center.z*center.z - m15/m11);
        }
    }
    
    public boolean containsPoint(Point3D p){
        //check if sphere contains point
        //false if point is exactly on surface of sphere
        double dist = p.getDistance(center);
        if(dist < radius){
            return true;
        }
        else{
            return false;
        }
    }
}
