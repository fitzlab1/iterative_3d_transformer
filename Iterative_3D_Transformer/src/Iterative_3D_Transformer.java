import ij.plugin.PlugIn;
import transformandoverlay_.TransformAndOverlayApp_;

public class Iterative_3D_Transformer implements PlugIn {

    //just a dummy class to launch the swing app from ImageJ
    
    public void run(String arg) {
        TransformAndOverlayApp_.main(new String[]{});
    }
}
