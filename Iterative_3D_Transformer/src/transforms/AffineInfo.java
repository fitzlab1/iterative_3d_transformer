package transforms;

import Jama.Matrix;

public class AffineInfo {
    //static methods that calculate some interesting properties of 
    //affine transforms for better user feedback
    
    public static String getWorstPointPair(Matrix srcPts, Matrix destPts, AffineTransform3D aff){
        //given a set of transformed points
        //determine which point pair is the worst
        //and by how much
        
        Matrix srcPtsTransformed = aff.applyToPoints(srcPts);
        
        StringBuilder s = new StringBuilder();

        int numPointPairs = Math.min(srcPtsTransformed.getRowDimension(), destPts.getRowDimension());
        int worstRow = 0;
        double worstResidual = 0;
        double meanResidual = 0;
        for(int i = 0; i < numPointPairs; i++){
            double residualX = Math.pow(srcPtsTransformed.get(i,0) - destPts.get(i, 0), 2);
            double residualY = Math.pow(srcPtsTransformed.get(i,1) - destPts.get(i, 1), 2);
            double residualZ = Math.pow(srcPtsTransformed.get(i,2) - destPts.get(i, 2), 2);
            double residual = Math.sqrt(residualX+residualY+residualZ);
            if(residual > worstResidual){
                worstResidual = residual;
                worstRow = i;
            }
            meanResidual += residual;
        }
        meanResidual = meanResidual / numPointPairs;
        
        //round to hundredths place
        meanResidual = Math.round(meanResidual*100.0)/100.0;
        worstResidual = Math.round(worstResidual*100.0)/100.0;
        
        int x1 = (int)Math.round(srcPts.get(worstRow, 0));
        int y1 = (int)Math.round(srcPts.get(worstRow, 1));
        int z1 = (int)Math.round(srcPts.get(worstRow, 2));
        
        int x2 = (int)Math.round(destPts.get(worstRow, 0));
        int y2 = (int)Math.round(destPts.get(worstRow, 1));
        int z2 = (int)Math.round(destPts.get(worstRow, 2));
        
        s.append("The worst pair of points is pair #" + (worstRow+1) + ": \n" );
        s.append("(" + x1 + "," + y1 + "," + z1 + ")");
        s.append("->(" + x2 + "," + y2 + "," + z2 + ")\n" );
        s.append("Residual of pair " + (worstRow+1) +": " + worstResidual + "\n");
        s.append("Mean residual of all point pairs: " + meanResidual + "\n");
        
        return s.toString();
    }
    
    
    
}
