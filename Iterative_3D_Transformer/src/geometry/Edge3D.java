/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import utils.ArrayUtils;

public class Edge3D implements Comparable {
    public Point3D[] points;
    public double length;
    
    public Edge3D(Point3D[] points_){
        points = points_;
        calcLength();
    }
    
    public void calcLength(){
        length = points[0].getDistance(points[1]);
    }
    
    @Override
    public int compareTo(Object o){
        if(!(o instanceof Edge3D)){
            System.out.println("Error: Can't compare Edge3D to " + o.getClass());
            return 0;
        }
        Edge3D e = (Edge3D) o;
        if(length > e.length){
            return 1;
        }
        else if(length == e.length){
            return 0;
        }
        else{
            return -1;
        }
    }
    
    public boolean matches(Edge3D e){
        //like equals, but allows points to be swapped in position
        return ArrayUtils.arrayMatches(points, e.points);
    }
    
    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Edge3D))
            return false;
        Edge3D e = (Edge3D) o;
        
        for(int i = 0 ; i < points.length; i++){
            if(! points[i].equals(e.points[i]))
                return false;
        }
        return true;
    }
    
    @Override public int hashCode(){
        //hashing assumes edges are undirected
        //doesn't matter which is point1 and which is point2.
        int result = points[0].hashCode() + points[1].hashCode();
        return result;
    }

}
