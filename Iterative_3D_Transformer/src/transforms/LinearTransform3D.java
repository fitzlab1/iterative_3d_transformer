/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package transforms;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import geometry.Point3D;
import ij.ImagePlus;
import ij.ImageStack;
import java.util.ArrayList;
import pointpatternmatcher_.Constants;

/**
 *
 * @author walkert
 */
public class LinearTransform3D extends Transform {
    
    private Matrix transform; //the actual transformation matrix
    private Matrix inverseTransform; //the inverse. Calculated during constructor.
    
        
    public LinearTransform3D(Matrix transformationMatrix){
        transform = transformationMatrix;
        inverseTransform = transform.inverse();
    }
    
    public LinearTransform3D(Matrix srcPts, Matrix destPts){
        //finds the affine transform matrix capable of turning srcPts into destPts.
        //srcPts and destPts must contain one point per row. 
        //They must also be padded on the right with ones. 
        //Format example:
        //srcPts = 
        // X0 Y0 Z0 1
        // X1 Y1 Z1 1
        // X2 Y2 Z2 1
        // X3 Y3 Z3 1
        // use the padWithOnes function to do this for your inputs if needed.
        
        // Using a more Procrustes-style algorithm, cause a full-on affine
        // is unstable as hell. I dunno what the other papers are smoking, but
        // affine isn't working for me. 

        //First, a little sanity checking of the inputs.
        int numDimensions = srcPts.getColumnDimension();
        if(numDimensions != 4){
            print("Expected 4 values per row but got " + 
                    numDimensions + ". Did you forget to pad with ones?");    
            return;
        }
        
        int numPoints = srcPts.getRowDimension();
        if(numPoints < 4){
            print("Only " + numPoints +
                    "points given. Need at least 4 to calculate transform.");   
            return;  
        }
        
        if(numPoints != destPts.getRowDimension() || numDimensions != destPts.getColumnDimension()){
            print("Error: srcPts and destPts must be the same size.");   
            return;
        }
        
        // now for the real stuff
        // subtract the first point to determine offset
        Point3D[] srcPts3D = Point3D.matrixToPointArray(srcPts);
        Point3D[] destPts3D = Point3D.matrixToPointArray(destPts);
        
        Point3D srcAvg = Point3D.getAverage(srcPts3D);
        Point3D destAvg = Point3D.getAverage(destPts3D);
        
        for(int i = 0; i < 4; i++){
            srcPts3D[i] = srcPts3D[i].subtract(srcAvg);
            destPts3D[i] = destPts3D[i].subtract(destAvg);
        }
        
        //now do an affine transform on those, and ignore any translational component (?)
        Matrix srcCentered = Point3D.pointArrayToMatrix(srcPts3D, false);
        Matrix destCentered = Point3D.pointArrayToMatrix(destPts3D, false);
        
        Matrix rotScale = srcPts.solve(destPts);
        
        
        
        Matrix c = (srcPts.transpose()).times(destPts);
        c = c.getMatrix(0, numDimensions-1, 0, c.getRowDimension()-1);
       
        Matrix Q = (srcPts.transpose()).times(srcPts);
        
        try{
            Matrix affineMatrix = Q.solve(c);
            transform = affineMatrix;
            inverseTransform = affineMatrix.inverse();  
        }
        catch(Exception ex){
            print("WARNING! SINGULAR MATRIX -- ADDING RANDOM EPSILONS TO FIX");
            //jiggle points slightly and try again. Most often there are 4 coplanar points here.
            for(int i = 0; i < srcPts.getColumnDimension(); i++){
                for(int j = 0; j < srcPts.getRowDimension()-1; j++){
                    double jiggled = srcPts.get(i, j) + Math.random() * Constants.EPSILON - Constants.EPSILON/2;
                    srcPts.set(i, j, jiggled);
                }
            }
            for(int i = 0; i < destPts.getColumnDimension(); i++){
                for(int j = 0; j < destPts.getRowDimension()-1; j++){
                    double jiggled = destPts.get(i, j) + Math.random() * Constants.EPSILON - Constants.EPSILON/2;
                    destPts.set(i, j, jiggled);
                }
            }
            Matrix affineMatrix = srcPts.solve(destPts);
            transform = affineMatrix;
            inverseTransform = affineMatrix.inverse();  
        }
    }

    public ImagePlus[] applyToImage(ImagePlus srcImp, ImagePlus destImp, boolean cropToDest){
       //doesn't do anything for images, this is only for working with points
        return null;
    }
    
    public Matrix applyToPoints(Matrix points){
        //apply affine transform to input points
        //return resulting point set
        return points.times(transform);
    }
    
    public Matrix applyInverseToPoints(Matrix points){
        //apply inverse transform to input points
        //return resulting point set
        return points.times(inverseTransform);
    }
    
    public double getZRotationAngle(){
        //find the rotational component of the transform
        //we don't care about translation here, it shouldn't be part of this
        Matrix noTranslation = transform.getMatrix(0, 2, 0, 2);
        
        SingularValueDecomposition svd = noTranslation.svd();
        Matrix rotMatrix = svd.getU().times(svd.getV().transpose());
        print("rotation matrix:");
        rotMatrix.print(8, 4);
        //start with a single vector pointing up in Z
        Point3D pointIn = new Point3D(new double[]{0,0,1});
        Matrix vecIn = pointIn.toMatrix(false);
        //apply rotation to that vector
        Matrix vecOut = vecIn.times(rotMatrix);
        //find the angle to where it landed
        Point3D pointOut = new Point3D(new double[]{vecOut.get(0,0),vecOut.get(0,1),vecOut.get(0,2)});
        return pointIn.angleBetween(pointOut);
    }
    
    private static void print(String s) {
        System.out.println(s);
    }

    public Matrix getTransform() {
        return transform;
    }
    public Matrix getInverseTransform() {
        return inverseTransform;
    }
}
