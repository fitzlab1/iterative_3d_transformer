package utils;

import utils.ExecTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArrayUtils {
    //static methods for matching object arrays
    //and finding objects that are elements of arrays
    //For small datasets, uses an n^2 method
    //For large datasets, uses a hashing method
    public static int methodCutoffPoint = 80; //where hash method catches up with n^2
    
    //A more intelligent way of setting the method cutoff point would be cool
    //since it can depend on the compiler and JVM.
    //This shouldn't change dependent on what the Object is too much, because
    //calculating hashCode() is generally about as hard as equals().
    
    public static boolean arrayContains(Object[] array, Object element){
        for(int i = 0; i < array.length; i++){
            if(element.equals(array[i])){
                return true;
            }            
        }
        return false;
    }
    
    public static boolean arrayContains(int[] array, int element){
        for(int i = 0; i < array.length; i++){
            if(element == array[i]){
                return true;
            }            
        }
        return false;
    }
    
    public static boolean arrayMatches(Object[] array1, Object[] array2){
        
        if(array1.length != array2.length){
            return false;
        }
        
        if(array1.length < methodCutoffPoint){
            return arrayMatchesNSquared(array1, array2);
        }
        else{
            return arrayMatchesHash(array1, array2);
        }
    }
    
    private static boolean arrayMatchesNSquared(Object[] array1, Object[] array2){
        //on objects that have compareTo, it's usually faster to sort
        //and then do matching on the sorted arrays.
        //This is an O(n^2) convenience method for matching two general
        //object arrays. Actually runs quite fast for an O(n^2).
        //Best used on small arrays.
        
        Object[] usedArray2 = Arrays.copyOf(array2, array2.length);
        for(int i = 0; i < array1.length; i++){
            boolean foundMatch = false;
            for(int j = 0; j < array2.length; j++){
                if(usedArray2[j] == null){
                    //don't use the same element twice
                    continue;
                }
                if(array1[i].equals(array2[j])){
                    foundMatch = true;
                    usedArray2[j] = null;
                    break;
                }
            }
            if(! foundMatch){
                //no match found for this element of array1
                return false;
            }
        }
        
        return true;
    }
            
    
    private static boolean arrayMatchesHash(Object[] array1, Object[] array2){
        //Hashes the objects in array2
        //Looks up each element of array1 in array2
        //removes objects from array2 as matches are found
        HashMap<Integer, ArrayList<Object>> hashOf2 = new HashMap<Integer, ArrayList<Object>>();
        for(int i = 0; i < array2.length; i++){
            int key = array2[i].hashCode();
            if(! hashOf2.containsKey(key)){
                hashOf2.put(key, new ArrayList<Object>());
            }
            hashOf2.get(key).add(array2[i]);
        }
        
        for(int i = 0; i < array1.length; i++){
            int key = array1[i].hashCode();
            if(! (hashOf2.get(key).remove(array1[i]))){
                return false;
            }
        }
        
        return true;
    }
    
    public static void testArrayMatching(){
        //multiply these to to get the dataset size.
        int numbersToTest = 40;
        int numCopies = 2;
        
        ExecTime et = new ExecTime();
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        ArrayList<Integer> list2 = new ArrayList<Integer>();
        
        for(int c = 0; c < numCopies; c++){
            for(int i = 0; i < numbersToTest; i++){
                list1.add(i);
            }
            for(int i = numbersToTest-1; i >= 0; i--){
                list2.add(i);
            }
        }        
        
        Integer[] arr1 = list1.toArray(new Integer[list1.size()]);
        Integer[] arr2 = list2.toArray(new Integer[list2.size()]);
        
        et.tic();
        if(arrayMatchesHash(arr1, arr2)){;            
            print("hash matcher worked. t = " + et.toc() + " nanoseconds.");
        }
        else{
            print("hash matcher error");
        }
        et.tic();
        if(arrayMatchesNSquared(arr1, arr2)){
            print("easy matcher worked. t = " + et.toc() + " nanoseconds.");
        }
        else{
            print("easy matcher error");
        }
    }
    
    public static void print(String s){
        System.out.println(s);
    }
}
