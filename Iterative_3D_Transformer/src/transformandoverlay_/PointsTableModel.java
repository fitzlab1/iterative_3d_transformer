/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package transformandoverlay_;

import geometry.Point3D;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author walkert
 */
public class PointsTableModel extends AbstractTableModel {
    
    ArrayList<Point3D> points = new ArrayList<Point3D>();
    
    ArrayList<String> columnNames = new ArrayList<String>();
    ArrayList<Class> columnClasses = new ArrayList<Class>();
    
    public PointsTableModel(){
        columnNames.add("#");
        columnNames.add("X");
        columnNames.add("Y");
        columnNames.add("Z");
        
        columnClasses.add(Integer.class);
        columnClasses.add(Double.class);
        columnClasses.add(Double.class);
        columnClasses.add(Double.class);
    }
    
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
    
    public String getColumnName(int column){
        return columnNames.get(column);
    }
    
    public int getRowCount() {
        return points.size();
    }

    public int getColumnCount() {
        return columnNames.size();
    }

    public Object getValueAt(int row, int col) {
        if(col == 0 && row < points.size()){
            return new Integer(row+1);
        }
        else if(col == 1){
            return points.get(row).x;
        }
        else if(col == 2){
            return points.get(row).y;
        }
        else if(col == 3){
            return points.get(row).z;
        }
        else{
            return null;
        }
    }
    
    public boolean isCellEditable(int row, int col){ 
        return false; 
    }
    
    public void addPoint(Point3D point){
        points.add(point);
        this.fireTableDataChanged();
    }
    
    public ArrayList<Point3D> getPoints() {
        return points;
    }
    
    public void setPoints(ArrayList<Point3D> points_) {
        this.points = points_;
        this.fireTableDataChanged();
    }
    
    public void addPoints(ArrayList<Point3D> points_){
        points.addAll(points_);
        this.fireTableDataChanged();
    }
    
    public void clearPoints(){
        points.clear();
        this.fireTableDataChanged();
    }
    
}
