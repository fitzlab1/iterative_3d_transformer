/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package transforms;

import Jama.Matrix;
import ij.ImagePlus;
import ij.process.ImageProcessor;

/**
 *
 * @author walkert
 */
public abstract class Transform {
    public abstract Matrix applyToPoints(Matrix pointsToTransform);
    public abstract Matrix applyInverseToPoints(Matrix pointsToInvert);
    public abstract ImagePlus[] applyToImage(ImagePlus src, ImagePlus dest, boolean cropToDest);
        
}
