/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package transformandoverlay_;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class WindowListModel extends AbstractListModel implements ComboBoxModel {
    
    private WindowListItem[] windowListItems = new WindowListItem[]{};
    private int selectedWindowID = 0; //use the window ID as primary key, not the title or index
    
    private WindowListItem getWindowByID(int id){
        for(int i = 0; i < windowListItems.length; i++){
            if(windowListItems[i].windowID == id){
                return windowListItems[i];
            }
        }
        return null;
    }
    
    
    public WindowListModel(){
        super();
    }

    @Override
    public WindowListItem getSelectedItem() {
        WindowListItem item = getWindowByID(selectedWindowID);
        return item;
    }
    
    public int getSelectedID(){
        return selectedWindowID;
    }
    
    public void setSelectedID(int id) {
        for(int i = 0; i < windowListItems.length; i++){
            if(windowListItems[i].windowID == id){
                setSelectedItem(windowListItems[i]);
            }
        }
        this.fireContentsChanged(this, 0, 0);
    }
      
    @Override
    public void setSelectedItem(Object newValue) {
        for(int i = 0; i < windowListItems.length; i++){
            if(windowListItems[i] == newValue){
                selectedWindowID = windowListItems[i].windowID;
            }
        }
        this.fireContentsChanged(this, 0, 0);
    }
    
    
    @Override
    public int getSize() {
        return windowListItems.length;
    }

    @Override
    public WindowListItem getElementAt(int i) {
        return windowListItems[i];
    }
    
    public void setWindowList(WindowListItem[] windowListItems_){
        windowListItems = windowListItems_;
        boolean selectedWindowStillExists = false;
        for(int i = 0; i < windowListItems.length; i++){
            if(windowListItems[i].windowID == selectedWindowID){
                selectedWindowStillExists = true;
            }
        }
        if(!selectedWindowStillExists){
            selectedWindowID = 0;
        }
        
        this.fireContentsChanged(this, 0, 0);
    }
    
}
