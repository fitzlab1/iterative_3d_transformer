package pointpatternmatcher_;

import transforms.PiecewiseAffineImageTransformer3D;
import transforms.AffineTransform3D;
import transforms.AffineTransform2D;
import utils.FileAndDirOperations;
import Jama.*; 
import transforms.PiecewiseAffineTransform3D;
import utils.MatrixMethods;
import geometry.Point3D;
import ij.ImagePlus;
import ij.ImageStack;
import ij.io.FileSaver;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author walkert
 */
public class PointPatternMatcher_ {

    public static void oldMain(String[] args) throws Exception{
        //testMatrixOperations();
        //DelaunayTester.test1tetra();
        //DelaunayTester.test2tetras();
        
        
        /*
        DelaunayTester dt = new DelaunayTester(20);
        dt.testPointRange(0, 50, 0, 50, 0, 50);
        System.exit(0);
        */
        
        //dt.testRandomPoints();
        //dt.testPointRange(-50, 50, -50, 50, -50, 50);
        
        //test of 2D affine transforms
        /*
        AffineTransform2D affine = new AffineTransform2D(srcPts, destPts);
        affine.getTransform().print(4, 8);
        Matrix srcTransformed = affine.applyTransform(srcPts);
        
        print("residual: " + affine.applyTransformAndGetResidual(srcPts, destPts));
        
        if(affine.getFlipType() == Constants.FLIP_NONE)
            print("flip: None");
        else if(affine.getFlipType() == Constants.FLIP_HORIZONTAL){
            print("flip: Horizontal");
        }
        else if(affine.getFlipType() == Constants.FLIP_VERTICAL){
            print("flip: Vertical");
        }
        print("angle: " + affine.getAngle());
        print("scale factor ratio: " + affine.getScaleRatio());
        */
        
        //test of image shit (2D)
        /*
        Opener o = new Opener();
        ImagePlus src = o.openImage("G:/netbeans-projects/PointPatternMatcher_/sample_data/2pt-frame50.tif");
        ImagePlus dest = o.openImage("G:/netbeans-projects/PointPatternMatcher_/sample_data/slice3-gcamp-11.tif");
        
        ImageProcessor[] results = affine.applyToImage(src.getProcessor(), dest.getProcessor());
        ImageProcessor srcImgTransformed = results[0];
        ImageProcessor destImgTranslated = results[1];
        
        ImagePlus plus1 = new ImagePlus("srcTransformed", srcImgTransformed);
        FileSaver fs1 = new FileSaver(plus1);
        fs1.saveAsTiff("G:/netbeans-projects/PointPatternMatcher_/sample_data/srcTransformed.tif");
        
        ImagePlus plus2 = new ImagePlus("destTranslated", destImgTranslated);
        FileSaver fs2 = new FileSaver(plus2);
        fs2.saveAsTiff("G:/netbeans-projects/PointPatternMatcher_/sample_data/destTranslated.tif");
        */
        
        //test of 3D affine transforms
        /*
        AffineTransform3D affine = new AffineTransform3D(srcPts, destPts);
        print("Affine transform:");
        affine.getTransform().print(8,4);
        Matrix srcTransformed = affine.applyTransform(srcPts);
        print("srcPts transformed:");
        srcTransformed.print(8,4);
        //print("residual: " + affine.applyTransformAndGetResidual(srcPts, destPts));
        
        print("inverse transform: ");
        affine.getInverseTransform().print(8,4);
        */
        //test of image shit (3D)
        //Matrix bestTransformMat = getMatrixFromFile("I:/F1069-alignment/matrix-3a.txt");
        
        
        /*
        double[][] srcPtsA = {
            {-5, 0, 0},
            {5, 0, 0},
            {0, 10, 0},
            {0, 0, 10}
        };
                
        double[][] destPtsA = {
            {-5,0,0},
            {5,0,0},
            {0,10,15.818},
            {0,-15.818,10}
        };
        
        Matrix srcPtsMatA = new Matrix(srcPtsA);
        Matrix destPtsMatA = new Matrix(destPtsA);
        srcPtsMatA = AffineTransform2D.padWithOnes(srcPtsMatA);
        destPtsMatA = AffineTransform2D.padWithOnes(destPtsMatA);
        
        AffineTransform3D aff = new AffineTransform3D(srcPtsMatA, destPtsMatA);
        
        String filePath = "I:/1080-rot/sec5-to-sec10";
        ArrayList<String> imagePaths = ImageFileIO.getImageFilesFromDir("I:/1080-rot/temp");
        
        ImageStack stackConfocalA = null;
        for(int i = 0; i < imagePaths.size(); i++){
            try{
                ImagePlus imp = ImageFileIO.readImageFromPath(imagePaths.get(i));
                if(stackConfocalA == null){
                    stackConfocalA = new ImageStack(imp.getWidth(), imp.getHeight());
                }
                //convert to 8-bit if needed
                ImageProcessor processor = imp.getProcessor();
                if(imp.getBitDepth() > 8){
                    processor = new ByteProcessor(imp.getBufferedImage());
                }
                stackConfocalA.addSlice(""+i, processor);
            }
            catch(Exception ex){
                print("Error reading from file: " + imagePaths.get(i));
                continue;
            }
        }
        ImageStack[] res = aff.applyToImage(stackConfocalA, stackConfocalA);
        ImageStack transformedStack = res[0];
        
        print("writing images...");
        for(int i = 0; i < transformedStack.getSize(); i++){
            ImageProcessor ip = transformedStack.getProcessor(i+1);
            ImagePlus imp = new ImagePlus("", ip);
            FileSaver fs = new FileSaver(imp);
            fs.saveAsTiff("G:/netbeans-projects/PointPatternMatcher_/sample_data/srcTransformed/" + String.format("%05d", i) + ".tif");
        }
        
        System.exit(0);
        */
        
        
        //get points
        double[][] srcPts = getSrcPoints();
        double[][] destPts = getDestPoints();
        
        //matrixify the points
        Matrix srcPtsMat = new Matrix(srcPts);
        Matrix destPtsMat = new Matrix(destPts);
        srcPtsMat = MatrixMethods.padWithOnes(srcPtsMat);
        destPtsMat = MatrixMethods.padWithOnes(destPtsMat);
        
        //Make Point3Ds out of them for tessellation use
        ArrayList<Point3D> srcPts3D = new ArrayList<Point3D>();
        ArrayList<Point3D> destPts3D = new ArrayList<Point3D>();
        for(int i = 0; i < srcPts.length; i++){
            Point3D srcPt = new Point3D(srcPts[i]);
            Point3D destPt = new Point3D(destPts[i]);
            srcPts3D.add(srcPt);
            destPts3D.add(destPt);
        }
        
        //calculate best-fit affine transform
        AffineTransform3D bestFitAffine = new AffineTransform3D(srcPtsMat, destPtsMat);
        
        //calculate tessellation
        //PiecewiseAffineTransform3D del = new PiecewiseAffineTransform3D(destPts3D,srcPts3D);
        PiecewiseAffineTransform3D del = null;
        
        //read in files
        ArrayList<String> imagePathsConfocal = ImageFileIO.getImageFilesFromDir("I:/F1069-alignment/Confocal/slice3-gcamp-fft");
        ArrayList<String> imagePaths2pt = ImageFileIO.getImageFilesFromDir("I:/F1069-alignment/2photon/z01-001-aligned-pretty-8bit/");
        ImageStack stack2pt = null;

        for(int i = 0; i < imagePaths2pt.size(); i++){
            try{
                ImagePlus imp = ImageFileIO.readImageFromPath(imagePaths2pt.get(i));
                if(stack2pt == null){
                    stack2pt = new ImageStack(imp.getWidth(), imp.getHeight());
                }
                //convert to 8-bit if needed
                ImageProcessor processor = imp.getProcessor();
                if(imp.getBitDepth() > 8){
                    processor = new ByteProcessor(imp.getBufferedImage());
                }
                stack2pt.addSlice(""+i, processor);
            }
            catch(Exception ex){
                print("Error reading from file: " + imagePaths2pt.get(i));
                continue;
            }
        }

        ImageStack stackConfocal = null;
        for(int i = 0; i < imagePathsConfocal.size(); i++){
            try{
                ImagePlus imp = ImageFileIO.readImageFromPath(imagePathsConfocal.get(i));
                if(stackConfocal == null){
                    stackConfocal = new ImageStack(imp.getWidth(), imp.getHeight());
                }
                //convert to 8-bit if needed
                ImageProcessor processor = imp.getProcessor();
                if(imp.getBitDepth() > 8){
                    processor = new ByteProcessor(imp.getBufferedImage());
                }
                stackConfocal.addSlice(""+i, processor);
            }
            catch(Exception ex){
                print("Error reading from file: " + imagePathsConfocal.get(i));
                continue;
            }
        }

        print("\nDelaunay points:");
        for(int i = 0; i < del.points.size(); i++){
            print("" + del.points.get(i));
        }
        print("");
                
        //run transform
        PiecewiseAffineImageTransformer3D imt = new PiecewiseAffineImageTransformer3D(bestFitAffine, del, stackConfocal, stack2pt, true);
        ImageStack[] results = imt.doTransform(Constants.NUM_THREADS);
        
        ImageStack srcTransformedStack = results[0];
        ImageStack destTranslatedStack = results[1];
        
        //write out results
        print("writing images...");
        for(int i = 0; i < srcTransformedStack.getSize(); i++){
            ImageProcessor ip = srcTransformedStack.getProcessor(i+1);
            ImagePlus imp = new ImagePlus("", ip);
            FileSaver fs = new FileSaver(imp);
            fs.saveAsTiff("G:/netbeans-projects/PointPatternMatcher_/sample_data/srcTransformed/" + String.format("%05d", i) + ".tif");
        }
        
        for(int i = 0; i < destTranslatedStack.getSize(); i++){
            ImageProcessor ip = destTranslatedStack.getProcessor(i+1);
            ImagePlus imp = new ImagePlus("", ip);
            FileSaver fs = new FileSaver(imp);
            fs.saveAsTiff("G:/netbeans-projects/PointPatternMatcher_/sample_data/destTranslated/" + String.format("%05d", i) + ".tif");
        }
        
        //srcTransformed.print(4, 8);
        //destPtsSub.print(4, 8);
    }
    
    
    public static double[][] getSrcPoints(){
        double[][] srcPts = {
            {1100.244424,1724.908684,10.60116744},
            {448.3694838,1554.824624,13.03924639},
            {1101.754584,1733.328907,9.387962896},
            {471.2592931,1599.056671,12.59786647},
            {1327.585172,1725.686539,7.865413919},
            {1294.113449,1782.21082,13.6734519},
            {985.9147135,1490.777122,3.233191203},
            {449.7255464,1942.969054,13.70080526},
            {1008,1330,4},
            {963,1602,9},
            {1336,1795,15},
            {598,884,14},
            {440.2365,1573.4246,12.5503},
            {1102,1738,10},
            {1207,1794,5},
            {418.9604074,705.5110186,15.3651214},
            {578.7406082,844.6543705,11.58845481},
            {519.875814,883.4153725,6.010942356},
            {676.6240066,801.286521,6.448523936},
            {544.117987,788.483593,8.794118389},
            {398.7141074,652.1249837,12.26396849},
            {603.0541029,1045.931652,21.5599065},
        };
        
        return srcPts;
    }
    
    
    public static double[][] getDestPoints(){
        double[][] destPts = {
            {359,692,51},
            {727,723,93},
            {358,695,53},
            {705,734,92},
            {245,646,43},
            {250,686,31},
            {460,577,59},
            {652,920,108},
            {478.0874692,491.8272397,44},
            {457.1294667,645.1398685,57},
            {228.3789711,690.7711161,25},
            {785.8999227,351.9505316,13},
            {725.3568976,728.704486,90},
            {357.0509076,695.3024243,51},
            {290.1816785,700.6415444,57},
            {905,297,45},
            {796,339,38},
            {820,356,63},
            {759,299,44},
            {825,309,53},
            {925,276,51},
            {753,447,42},
        };
        
        return destPts;
    }
        
    
    
   public static void testMatrixOperations(){
   /* 
    | Tests LU, QR, SVD and symmetric Eig decompositions.
    |
    |   n       = order of magic square.
    |   trace   = diagonal sum, should be the magic sum, (n^3 + n)/2.
    |   max_eig = maximum eigenvalue of (A + A')/2, should equal trace.
    |   rank    = linear algebraic rank,
    |             should equal n if n is odd, be less than n if n is even.
    |   cond    = L_2 condition number, ratio of singular values.
    |   lu_res  = test of LU factorization, norm1(L*U-A(p,:))/(n*eps).
    |   qr_res  = test of QR factorization, norm1(Q*R-A)/(n*eps).
    */

      print("\n    Test of Matrix Class, using magic squares.\n");
      print("    See MagicSquareExample.main() for an explanation.\n");
      print("\n      n     trace       max_eig   rank        cond      lu_res      qr_res\n\n");
 
      Date start_time = new Date();
      double eps = Math.pow(2.0,-52.0);
      for (int n = 3; n <= 32; n++) {
         print(fixedWidthIntegertoString(n,7));

         Matrix M = magic(n);

         int t = (int) M.trace();
         print(fixedWidthIntegertoString(t,10));

         EigenvalueDecomposition E =
            new EigenvalueDecomposition(M.plus(M.transpose()).times(0.5));
         double[] d = E.getRealEigenvalues();
         print(fixedWidthDoubletoString(d[n-1],14,3));

         int r = M.rank();
         print(fixedWidthIntegertoString(r,7));

         double c = M.cond();
         print(c < 1/eps ? fixedWidthDoubletoString(c,12,3) :
            "         Inf");

         LUDecomposition LU = new LUDecomposition(M);
         Matrix L = LU.getL();
         Matrix U = LU.getU();
         int[] p = LU.getPivot();
         Matrix R = L.times(U).minus(M.getMatrix(p,0,n-1));
         double res = R.norm1()/(n*eps);
         print(fixedWidthDoubletoString(res,12,3));

         QRDecomposition QR = new QRDecomposition(M);
         Matrix Q = QR.getQ();
         R = QR.getR();
         R = Q.times(R).minus(M);
         res = R.norm1()/(n*eps);
         print(fixedWidthDoubletoString(res,12,3));

         print("\n");
      }
      Date stop_time = new Date();
      double etime = (stop_time.getTime() - start_time.getTime())/1000.;
      print("\nElapsed Time = " + 
         fixedWidthDoubletoString(etime,12,3) + " seconds\n");
      print("Adios\n");
   }
    
   public static Matrix magic(int n) {

      double[][] M = new double[n][n];

      // Odd order

      if ((n % 2) == 1) {
         int a = (n+1)/2;
         int b = (n+1);
         for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
               M[i][j] = n*((i+j+a) % n) + ((i+2*j+b) % n) + 1;
            }
         }

      // Doubly Even Order

      } else if ((n % 4) == 0) {
         for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
               if (((i+1)/2)%2 == ((j+1)/2)%2) {
                  M[i][j] = n*n-n*i-j;
               } else {
                  M[i][j] = n*i+j+1;
               }
            }
         }

      // Singly Even Order

      } else {
         int p = n/2;
         int k = (n-2)/4;
         Matrix A = magic(p);
         for (int j = 0; j < p; j++) {
            for (int i = 0; i < p; i++) {
               double aij = A.get(i,j);
               M[i][j] = aij;
               M[i][j+p] = aij + 2*p*p;
               M[i+p][j] = aij + 3*p*p;
               M[i+p][j+p] = aij + p*p;
            }
         }
         for (int i = 0; i < p; i++) {
            for (int j = 0; j < k; j++) {
               double t = M[i][j]; M[i][j] = M[i+p][j]; M[i+p][j] = t;
            }
            for (int j = n-k+1; j < n; j++) {
               double t = M[i][j]; M[i][j] = M[i+p][j]; M[i+p][j] = t;
            }
         }
         double t = M[k][0]; M[k][0] = M[k+p][0]; M[k+p][0] = t;
         t = M[k][k]; M[k][k] = M[k+p][k]; M[k+p][k] = t;
      }
      return new Matrix(M);
   }

   /** Shorten spelling of print. **/

   private static void print (String s) {
      System.out.println(s);
   }
   
   /** Format double with Fw.d. **/

   public static String fixedWidthDoubletoString (double x, int w, int d) {
      java.text.DecimalFormat fmt = new java.text.DecimalFormat();
      fmt.setMaximumFractionDigits(d);
      fmt.setMinimumFractionDigits(d);
      fmt.setGroupingUsed(false);
      String s = fmt.format(x);
      while (s.length() < w) {
         s = " " + s;
      }
      return s;
   }

   /** Format integer with Iw. **/

   public static String fixedWidthIntegertoString (int n, int w) {
      String s = Integer.toString(n);
      while (s.length() < w) {
         s = " " + s;
      }
      return s;
   }

   public static Matrix getMatrixFromFile(String filepath){
       // opens a file, reads in matrix. Assumes matrix was copy-pasted from Matlab into the file. 
       String matStr = FileAndDirOperations.readFileIntoString(filepath);
       String[] lines = matStr.split("\\n");
       
       ArrayList<Double> doubleList = new ArrayList<Double>();
       int entriesPerLine = 0;
       for(int i = 0; i < lines.length; i++){
           //convert whitespace to commas first; allows processing of CSVs too.
           String line = lines[i].trim().replaceAll("\\s+", ",");
           String[] toks = line.split(",");
           if(toks.length > 1){
               entriesPerLine = toks.length;
           }
           for(int j = 0; j < toks.length; j++){
               if(!toks[j].trim().isEmpty()){
                   Double x = Double.parseDouble(toks[j]);
                   doubleList.add(x);
               }
           }
       }
       
       int numLines = doubleList.size() / entriesPerLine;
       double[][] doubleMat = new double[numLines][entriesPerLine];
       for(int i = 0; i < numLines; i++){
           for(int j = 0; j < entriesPerLine; j++){
               doubleMat[i][j] = doubleList.get(j+i*entriesPerLine);
           }
       }
       
       return new Matrix(doubleMat);       
   }
}
