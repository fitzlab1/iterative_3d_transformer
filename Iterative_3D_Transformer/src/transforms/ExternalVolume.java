package transforms;

import geometry.Plane3D;
import geometry.Point3D;

public class ExternalVolume {
    /*  Tessellations are only defined out to the convex hull of a point set.
     *  The infinite space outside of the convex hull can be divided up 
     *  into several ExternalVolumes. Each ExternalVolume is attached to 
     *  one face of one tetrahedron on the convex hull.
     *  That triangular face forms the "top" of the external volume.
     *  The volume is bounded on three other sides by planes extending from
     *  the three edges of the top face. These planes may converge, but
     *  often they spread outwards infinitely.
     */ 
    
    public Plane3D hullFace;
    public int hullTetraIndex;
    
    public Plane3D[] boundingPlanes;
    int[] neighbors; //external volumes that border this one
    
    boolean visited = false; //used in searching.
    
    public AffineTransform3D tform;
    
    public ExternalVolume(Plane3D hullFace_, Point3D normalDirection){
        hullFace = new Plane3D(hullFace_.points);
        if(hullFace.normalPointsAwayFrom(normalDirection)){
            hullFace.flipNormal();
        }
        boundingPlanes = new Plane3D[3];
        for(int i = 0; i < 3; i++){
            boundingPlanes[i] = new Plane3D();
        }
        neighbors = new int[] {-1,-1,-1};        
    }
    
    public boolean containsPoint(Point3D p){
        //returns whether the external volume contains p.
        //Note that points are considered inside a volume if they are
        //on any of the bounding planes or top.
        if(! (hullFace.normalPointsAwayFrom(p) || hullFace.isOnPlane(p))){
            return false;
        }
        for(int i = 0; i < boundingPlanes.length; i++){
            if(! (boundingPlanes[i].normalPointsAwayFrom(p) || boundingPlanes[i].isOnPlane(p))){
                return false;
            }
        }
        return true;
    }
    
    
    public int[] findPoint(Point3D p, int myIndex, int numTetras){
        //returns indices of neighboring tetras that might contain p.
        //considers neighbors on all 4 faces of externalVolume, so returns 4 values.
        //the first 3 are other externalVolumes, the last is a tetrahedron.
        //value = neighborIndex indicates that we should look towards that neighbor.
        //value = myIndex indicates that neighbor cannot contain p.
        //so, if all 4 values returned are myIndex, p is in this externalVolume.
        
        int[] neighborsTowardsP = new int[4];
        
        //check externalVolume neighbors
        for(int i = 0; i < 3; i++){
            if(boundingPlanes[i].normalPointsAwayFrom(p) || boundingPlanes[i].isOnPlane(p)){
                //p is on the inside of this boundingPlane; neighbor
                //cannot contain p
                neighborsTowardsP[i] = myIndex;     
            }
            else{        
                //neighbor may contain p
                //add numTetras to it to tell the search algorithm
                //that neighbor is not a tetra
                neighborsTowardsP[i] = neighbors[i] + numTetras;
            }
        }
        
        //check tetra on hullface
        if(hullFace.normalPointsAwayFrom(p) || hullFace.isOnPlane(p)){
            //p is on the inside of hullFace; neighbor
            //cannot contain p
            neighborsTowardsP[3] = myIndex;
        }
        else{
            //neighbor may contain p
            neighborsTowardsP[3] = hullTetraIndex;
        }
        
        return neighborsTowardsP;
    }
}
