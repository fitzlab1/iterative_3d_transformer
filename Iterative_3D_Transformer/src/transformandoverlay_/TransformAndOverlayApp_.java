/*
 * TransformAndOverlayApp_.java
 */

package transformandoverlay_;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.EventObject;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class TransformAndOverlayApp_ extends SingleFrameApplication {
    private TransformAndOverlay_View view;
            
    @Override protected void startup() {
         view = new TransformAndOverlay_View(this);
        
         
        addExitListener(new ExitListener() {
            public boolean canExit(EventObject e) {
                view.getFrame().dispose();
                return false;
            }
            public void willExit(EventObject event) {
            }
        });
        
        show(view);
    }
    
    @Override
    protected void end() {
        JFrame mainFrame = getMainFrame();
        if (mainFrame != null || mainFrame.isDisplayable()) {
            mainFrame.setVisible(false);
            mainFrame.dispose();
        }
        super.end();
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {

        //Prevent exiting imageJ on close by trapping the window close event
        root.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                view.getFrame().dispose();
            }
        });
    }
    
    /**
     * A convenient static getter for the application instance.
     * @return the instance of TransformAndOverlayApp_
     */
    public static TransformAndOverlayApp_ getApplication() {
        return Application.getInstance(TransformAndOverlayApp_.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        launch(TransformAndOverlayApp_.class, args);
    }
}
