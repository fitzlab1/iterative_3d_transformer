
package geometry;

public class Box3D {
    //A rectangular prism shaped volume.
    
    public Plane3D[] surfaces;
    public Point3D[] corners;
    
    public Box3D(Plane3D[] surfaces_, Point3D[] corners_){
        surfaces = surfaces_;
        corners = corners_;
    }
    
    public Box3D(Point3D[] corners_){
        //Can provide corners in arbitrary order.
        //Box is required to be shaped like a rectangular prism; 
        //parallelepipeds are no good!
        corners = corners_;
        
        for(int i = 0; i < corners.length; i++){
            print("corner " + i + ": " + corners[i].x + "    " + corners[i].y + "    " + corners[i].z);
        }
        
        //take all possible subsets of 3 points
        //make a plane out of the 3 points
        //if all other points are either on that plane or behind it, we have 
        //a surface plane
        surfaces = new Plane3D[6];
        int surfaceIndex = 0;
        for(int c0 = 0; c0 < 8; c0++){
           for(int c1 = c0+1; c1 < 8; c1++){
               for(int c2 = c1+1; c2 < 8; c2++){
                   Plane3D plane = new Plane3D(new Point3D[]{corners[c0], corners[c1], corners[c2]});
                   
                   boolean isSurfacePlane = true;
                   boolean normalSwitchedAlready = false; //we are allowed to swap the normal just once
                   
                   for(int c3 = 0; c3 < 8; c3++){
                       if(c3 == c2 || c3 == c1 || c3 == c0)
                           continue;
                       if(plane.isOnPlane(corners[c3]))
                           continue;
                       if(!plane.normalPointsAwayFrom(corners[c3])){
                           //normal is pointing in the wrong direction
                           //fix it if possible
                           if(! normalSwitchedAlready){
                               plane.normal.flip();
                               normalSwitchedAlready = true;
                           }
                           else{
                               //we already flipped this normal to fit some other
                               //point; this can't be a surface plane
                               isSurfacePlane = false;
                               break;
                           }
                       }
                       else{
                           //normal doesn't need switching for this point
                           //don't switch it for any future points
                           normalSwitchedAlready = true; 
                       }
                   }
                   
                   if(isSurfacePlane){
                       //check that we're not equal to any previous surface plane
                       boolean repeatedPlane = false;
                       for(int s = 0; s < surfaceIndex; s++){
                           if(surfaces[s].normal.isExtremelyCloseTo(plane.normal)){
                               repeatedPlane = true;
                               break;
                           }
                       }
                       if(! repeatedPlane){
                           print("surface: " + c0 + " " + c1 + " " + c2);
                           print("normal: " + plane.normal.toString());
                           surfaces[surfaceIndex] = plane;
                           surfaceIndex++;
                       }
                   }
               } 
           } 
        }
    }
    
    public Box3D(Plane3D[] surfaces_){
        surfaces = surfaces_;
        //determine corner points
        //every possible intersection of 3 planes that results in a point
        //will form one of the 8 corners.
        //Assumes that opposite surfaces of the box are parallel to each other.
        corners = new Point3D[8];
        int cornerIndex = 0;
        for(int i = 0; i < surfaces.length; i++){
            for(int j = i+1; j < surfaces.length; j++){
                for(int k = j+1; k < surfaces.length; k++){
                    Point3D intersectionPoint = surfaces[i].intersectPlanes(surfaces[j], surfaces[k]);
                    if(intersectionPoint != null){
                        corners[cornerIndex] = intersectionPoint;
                        cornerIndex++;
                    }
                }
            }
        }
    }
    
    public void flipNormals(){
        //Want an inside out box? We can do that!
        for(int s = 0; s < surfaces.length; s++){
            surfaces[s].normal.flip();
        }
    }
    
    public static void print(String s){
        System.out.println(s);
    }
}
