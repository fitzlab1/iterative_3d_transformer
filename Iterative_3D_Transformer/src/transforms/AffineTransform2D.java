/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package transforms;
import Jama.*; 
import ij.ImagePlus;
import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;

/**
 * @author walkert
 */
public class AffineTransform2D extends Transform{
    
    private Matrix transform; //the actual transformation matrix
    private Matrix inverseTransform; //the inverse. Calculated during constructor.

    public AffineTransform2D(Matrix transformationMatrix){
        transform = transformationMatrix;
    }
    
    public AffineTransform2D(Matrix srcPts, Matrix destPts){
        //finds the affine transform matrix capable of turning srcPts into destPts.
        //srcPts and destPts must contain one point per row. 
        //They must also be padded on the right with ones. 
        //Format example:
        //srcPts = 
        // X0 Y0 1
        // X1 Y1 1
        // X2 Y2 1
        // X3 Y3 1
        // use the padWithOnes function to do this for your inputs if needed.
        
        // Using algorithm from "Fitting affine and orthogonal transformations
        // between two sets of points", by Helmuth Spath. See bottom of page 28
        // from that paper for specifics.

        //First, a little sanity checking of the inputs.
        int numDimensions = srcPts.getColumnDimension();
        if(numDimensions != 3){
            print("Expected 3 values per row but got " + 
                    numDimensions + ". Did you forget to pad with ones?"); 
            return;           
        }
        
        int numPoints = srcPts.getRowDimension();
        if(numPoints < 3){
            print("Only " + numPoints +
                    "points given. Need at least 3 to calculate transform.");   
            return;
        }
        
        if(numPoints != destPts.getRowDimension() || numDimensions != destPts.getColumnDimension()){
            print("Error: srcPts and destPts must be the same size."); 
            return;
        }
        
        // Now, the least squares affine calculation
        Matrix c = (srcPts.transpose()).times(destPts);
        c = c.getMatrix(0, numDimensions-1, 0, c.getRowDimension()-1);
        Matrix Q = (srcPts.transpose()).times(srcPts);
        Matrix affineMatrix = srcPts.solve(destPts);

        transform = affineMatrix;
        inverseTransform = affineMatrix.inverse();
    }

    
    public ImagePlus[] applyToImage(ImagePlus srcImp, ImagePlus destImp, boolean cropToDest){
        //applies the affine transform to a source image (bilinear interpolation)
        //returns source image transformed
        //also returns a translation of dest image for easy creation of overlays.
        //operates on 8-bit only! Will overflow otherwise.
        
        ImageStack srcTransformedStack = null;
        ImageStack destTranslatedStack = null;
        
        for(int slice = 0; slice < srcImp.getStack().getSize(); slice++){
            ImageProcessor src = srcImp.getImageStack().getProcessor(slice+1);
            ImageProcessor dest = destImp.getImageStack().getProcessor(1);

            //determine size of output image. 
            //Needs to at least hold destImage.
            double outMinX = 0;
            double outMinY = 0;
            double outMaxX = dest.getWidth();
            double outMaxY = dest.getHeight();

            //apply forward transform to the src image corners
            //if it goes outside the bounds of destImage, extend outImage in that direction
            double sw = src.getWidth();
            double sh = src.getHeight();
            double[][] srcCornersDouble = {
                {0,0,1},
                {sw,0,1},
                {0,sh,1},
                {sw,sh,1}
            };
            Matrix srcCorners = new Matrix(srcCornersDouble);
            Matrix srcCornersAffine = this.applyToPoints(srcCorners);
            for(int i = 0; i < srcCorners.getRowDimension(); i++){
                print("corner x: " + srcCornersAffine.get(i,0) + " y: " + srcCornersAffine.get(i,1));
                if(srcCornersAffine.get(i,0) < outMinX){
                    outMinX = srcCornersAffine.get(i,0);
                } 
                if(srcCornersAffine.get(i,0) > outMaxX){
                    outMaxX = srcCornersAffine.get(i,0);
                } 
                if(srcCornersAffine.get(i,1) < outMinY){
                    outMinY = srcCornersAffine.get(i,1);
                } 
                if(srcCornersAffine.get(i,1) > outMaxY){
                    outMaxY = srcCornersAffine.get(i,1);
                } 
            }

            int outImageWidth = (int) Math.ceil(outMaxX - outMinX);
            int outImageHeight = (int) Math.ceil(outMaxY - outMinY);

            //BUT! All this assumes we're not cropping the output image.
            //If we are, it's much easier:
            if(cropToDest){
                //width height and depth are defined solely by the dest image:
                outImageWidth = dest.getWidth();
                outImageHeight = dest.getHeight();
            }

            print("width: " + outImageWidth + " height: " + outImageHeight);

            ImageProcessor destTranslated = new ByteProcessor(outImageWidth,outImageHeight);
            ImageProcessor srcTransformed = new ByteProcessor(outImageWidth,outImageHeight);

            //now fill up each image with pixel values

            //dest image will move iff srcTransform goes below 0.
            int destTranslateX = (int) Math.round(0-outMinX);
            int destTranslateY = (int) Math.round(0-outMinY);

            for(int i = 0; i < dest.getHeight(); i++){
                for(int j = 0; j < dest.getWidth(); j++){
                    int pixelValue = dest.get(j, i);
                    if(cropToDest){
                        destTranslated.set(j, i, pixelValue);
                    }
                    else{
                        destTranslated.set(j+destTranslateX, i+destTranslateY, pixelValue);
                    }
                }
            }

            //now do bilinear interpolation to transform srcImage
            for(int i = 0; i < outImageHeight; i++){
                for(int j = 0; j < outImageWidth; j++){

                    Matrix inputSpot;
                    if(cropToDest){
                        double[][] outPixelSpot = {{j,i,1}};
                        Matrix outPixelSpotMat = new Matrix(outPixelSpot);
                        inputSpot = outPixelSpotMat.times(inverseTransform);

                    }
                    else{
                        //no cropping
                        double[][] outPixelSpot = {{j-destTranslateX,i-destTranslateY,1}};
                        Matrix outPixelSpotMat = new Matrix(outPixelSpot);
                        inputSpot = outPixelSpotMat.times(inverseTransform);
                    }

                    double x = inputSpot.get(0,0);
                    double y = inputSpot.get(0,1);

                    int xLow = (int) Math.floor(x);
                    int xHigh = xLow+1;
                    int yLow = (int) Math.floor(y);
                    int yHigh = yLow+1;

                    //bounds check - make sure we're in the source image
                    if(xHigh < sw && xLow > 0 && yHigh < sh && yLow > 0){
                        //interpolate nearby pixels
                        double leftUp = src.get(xLow,yLow);
                        double rightUp = src.get(xHigh,yLow);
                        double leftDown = src.get(xLow,yHigh);
                        double rightDown = src.get(xHigh,yHigh);
                        double xOffset = x-xLow;
                        double yOffset = y-yLow;
                        double xAvg1 = leftUp + xOffset*(rightUp-leftUp);
                        double xAvg2 = leftDown + xOffset*(rightDown-leftDown);
                        int pixelValue = (int) Math.round(xAvg1 + yOffset*(xAvg2-xAvg1));
                        srcTransformed.set(j, i, pixelValue);
                    }
                    else{
                        //not in source image, so output pixel can stay black
                        continue;
                    }
                }
            }
            
            if(srcTransformedStack == null){
                srcTransformedStack = new ImageStack(srcTransformed.getWidth(), srcTransformed.getHeight());
            }
            if(destTranslatedStack == null){
                destTranslatedStack = new ImageStack(destTranslated.getWidth(), destTranslated.getHeight());
            }
            srcTransformedStack.addSlice(srcTransformed);
            destTranslatedStack.addSlice(destTranslated);
        }
        
        ImagePlus srcTransformedImp = new ImagePlus("Source Transformed", srcTransformedStack);
        ImagePlus destTranslatedImp = new ImagePlus("Destination Translated", destTranslatedStack);
        return new ImagePlus[] {srcTransformedImp,destTranslatedImp};
    }
    
    public Matrix applyToPoints(Matrix points){
        //apply affine transform to input points
        //return resulting point set
        return points.times(transform);
    }
    
    public Matrix applyInverseToPoints(Matrix points){
        //apply inverse transform to input points
        //return resulting point set
        return points.times(inverseTransform);
    }
    
    
    private static void print(String s) {
        System.out.println(s);
    }
    
    public Matrix getTransform() {
        return transform;
    }
    public Matrix getInverseTransform() {
        return inverseTransform;
    }
}
