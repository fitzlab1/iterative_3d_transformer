package transforms;

import Jama.Matrix;
import geometry.Box3D;
import geometry.Plane3D;
import geometry.Point3D;
import geometry.Ray3D;
import geometry.Sphere;
import ij.ImagePlus;
import ij.ImageStack;
import java.util.ArrayList;
import java.util.Stack;
import pointpatternmatcher_.Constants;
import utils.ArrayUtils;
import utils.FileAndDirOperations;

/*
 * @author walkert
 */
public class PiecewiseAffineTransform3D extends Transform {
    //Produces Delaunay tessellations (3D)
    //tessellations are guaranteed to be valid
    //right now the calculation of a tessellation is super slow
    //but point location is pretty fast (and that's much more important). 

    public ArrayList<TessTetrahedron> tetras = new ArrayList<TessTetrahedron>();
    public ArrayList<ExternalVolume> externalVolumes = new ArrayList<ExternalVolume>();
    public ArrayList<Point3D> points = new ArrayList<Point3D>();
    public ArrayList<Point3D> destPoints = new ArrayList<Point3D>();
    
    
    //When we look for the tetrahedron that contains a given point,
    //we should start the search in the last place we were. 
    //Speeds up point searches a ton, and we do lots and lots of those.
    int lastPointSearchResult = 0;

    public AffineTransform3D bullshit = new AffineTransform3D(
            new Matrix(new double[][] {
                {-1000, 0, 0, 0},
                {0, -1000, 0, 0},
                {0, 0, -1000, 0},
                {99999, 999999, 999999, 1},
            }));
    
    
    public PiecewiseAffineTransform3D(ArrayList<Point3D> srcPointSet, ArrayList<Point3D> destPointSet, Box3D imageBox) {
        points = srcPointSet;
        destPoints = destPointSet;
        
        
        makeDelaunayTetras();
        print("Tetras found.");
        fillInNeighbors();
        print("Made transforms.");
        makeTransforms();
        print("Filled in neighbors.");
        
        //makeDelaunayTetrasAndExtrapolate(imageBox);
        
        //findExternalVolumes();
        print("Found external volumes.\n");
        
        print("Delaunay tetras: " + tetras.size());
        print("External volumes: " + externalVolumes.size() + "\n");
    }
    
    public ImagePlus[] applyToImage(ImagePlus srcImp, ImagePlus destImp, boolean cropToDest){
        Matrix srcPtsMat = Point3D.pointArrayToMatrix(points.toArray(new Point3D[]{}), true);
        Matrix destPtsMat = Point3D.pointArrayToMatrix(destPoints.toArray(new Point3D[]{}), true);
        AffineTransform3D bestFitAffine = new AffineTransform3D(srcPtsMat,destPtsMat);
        PiecewiseAffineImageTransformer3D imt = new PiecewiseAffineImageTransformer3D(bestFitAffine, this, srcImp.getImageStack(), destImp.getImageStack(), true);
        ImageStack[] results = imt.doTransform(Constants.NUM_THREADS);
        
        ImagePlus srcTransformed = new ImagePlus("Source Transformed", results[0]);
        ImagePlus destTranslated = new ImagePlus("Destination Translated", results[1]);
        
        return new ImagePlus[]{srcTransformed, destTranslated};
    }

    public Matrix applyInverseToPoints(Matrix pointsToInvert) {
        //look up what transform to apply to each point and do so.
        Point3D[] pointsArray = Point3D.matrixToPointArray(pointsToInvert);
        Matrix invertedPoints = new Matrix(pointsArray.length, 4);
        for(int i = 0; i < pointsArray.length; i++){
            //get transform for point
            AffineTransform3D tform = getTransform(pointsArray[i]);
            Matrix pMat = Point3D.pointArrayToMatrix(new Point3D[]{pointsArray[i]}, true);
            Matrix invertedPointMat = tform.applyToPoints(pMat);
            invertedPoints.set(i, 0, invertedPointMat.get(0, 0));
            invertedPoints.set(i, 1, invertedPointMat.get(0, 1));
            invertedPoints.set(i, 2, invertedPointMat.get(0, 2));
            invertedPoints.set(i, 3, invertedPointMat.get(0, 3));
        }
        return invertedPoints;
    }

    public Matrix applyToPoints(Matrix pointsToTransform) {
        //look up what transform to apply to each point and do so.
        Point3D[] pointsArray = Point3D.matrixToPointArray(pointsToTransform);
        Matrix transformedPoints = new Matrix(pointsArray.length, 4);
        for(int i = 0; i < pointsArray.length; i++){
            //get transform for point
            AffineTransform3D tform = getTransform(pointsArray[i]);
            Matrix pMat = Point3D.pointArrayToMatrix(new Point3D[]{pointsArray[i]}, true);
            Matrix transformedPointMat = tform.applyInverseToPoints(pMat);
            transformedPoints.set(i, 0, transformedPointMat.get(0, 0));
            transformedPoints.set(i, 1, transformedPointMat.get(0, 1));
            transformedPoints.set(i, 2, transformedPointMat.get(0, 2));
            transformedPoints.set(i, 3, transformedPointMat.get(0, 3));
        }
        return transformedPoints;
    }
    
    
    
    public AffineTransform3D getTransform(Point3D p) {
        //BFS over neighbor tetras / externalVolumes to get index of the one 
        //containing p. Then look up what the transform of it is.
        //Can start at any tetra or externalVolume.
        
        //NOT THREAD SAFE! NOT EVEN REMOTELY THREAD SAFE! WATCH THE FUCK OUT!
        
        //For convenience, we are going to treat externalVolumes as being
        //tetras here; we will offset their index by tetras.size().
        int numTetras = tetras.size();
        
        Stack<Integer> indicesToCheck = new Stack<Integer>();

        //we'll be marking tetras as "visited" on our way
        //make sure to unmark them afterwards!
        ArrayList<Integer> indicesToUnvisit = new ArrayList<Integer>();
        
        //start in the last place we found a point
        indicesToCheck.add(lastPointSearchResult);
        
        //we are visiting this spot
        if(lastPointSearchResult < numTetras){
            tetras.get(lastPointSearchResult).visited = true;
        }
        else{
            externalVolumes.get(lastPointSearchResult-numTetras).visited = true;
        }
        indicesToUnvisit.add(lastPointSearchResult);
        //print("new search");
        int searchResult = -2; //dummy value to make the compiler happy; will be replaced during search
        while (!indicesToCheck.isEmpty()) {
            int index = indicesToCheck.pop();
            int[] neighborsToCheck;
            int votesForThisIndex = 0;
            
            if(index < numTetras){
                //we're at a tetrahedron
                //look through its neighbors to figure out if we're at the right
                //spot, or where we go next
                //print("\tat tetra: " + index);
                neighborsToCheck = tetras.get(index).findPoint(p, index, numTetras);
            }
            else{
                //we're at an externalVolume
                //look through its neighbors to figure out if we're at the right
                //spot, or where we go next
                int evIndex = index - numTetras;
                //print("\tat ev: " + evIndex);
                neighborsToCheck = externalVolumes.get(evIndex).findPoint(p, index, numTetras);
            }
                
            for (int i = 0; i < neighborsToCheck.length; i++) {
                int neighborIndex = neighborsToCheck[i];
                if (neighborsToCheck[i] == index) {
                    votesForThisIndex++;
                }
                else if(neighborIndex < numTetras && !tetras.get(neighborsToCheck[i]).visited){
                    //add neighbor tetra to the list
                    indicesToCheck.add(neighborIndex);
                    //mark it so we don't search it again
                    tetras.get(neighborsToCheck[i]).visited = true;
                    indicesToUnvisit.add(neighborIndex);
                }
                else if(neighborIndex >= numTetras && !externalVolumes.get(neighborIndex-numTetras).visited){
                    //add neighbor externalVolume to the list
                    indicesToCheck.add(neighborIndex);
                    //mark it so we don't search it again
                    externalVolumes.get(neighborIndex-numTetras).visited = true;
                    indicesToUnvisit.add(neighborIndex);
                }
            }
            //print("\t\tvotes: " + votesForThisIndex);
            if (votesForThisIndex == 4) {
                //we found it! Point is in here.
                searchResult = index;
                break;
            }
            
        }

        if(searchResult == -2){
            //print("================");
            
            ArrayList<Integer> pointInTetras = tetrasContainingPoint(p);
            ArrayList<Integer> pointInEvs = evsContainingPoint(p);
            
            if(pointInTetras.isEmpty() && pointInEvs.isEmpty()){
                //print("" + p);
            }
            
            //try offsetting the point a little, see if that helps...
            Point3D q = p.copy();
            q.x += 0.1;
            q.y += 0.1;
            q.z += 0.1;
            pointInTetras = tetrasContainingPoint(q);
            pointInEvs = evsContainingPoint(q);
            
            if(pointInTetras.isEmpty() && pointInEvs.isEmpty()){
                //print("" + q);
            }     
            //print("================");
             searchResult = lastPointSearchResult;
        }
        
        //unvisit tetras we went through
        for (int i = 0; i < indicesToUnvisit.size(); i++) {
            int index = indicesToUnvisit.get(i);
            if(index < numTetras){
                tetras.get(index).visited = false;
            }
            else{
                externalVolumes.get(index-numTetras).visited = false;
            }
        }
        
        //return search result
        lastPointSearchResult = searchResult;
        if(searchResult < numTetras){            
            //if(!tetras.get(searchResult).containsPoint(p)){
            //    print("Tetra containsPoint error!" +p);
                //return bullshit;
            //}
            return tetras.get(searchResult).tform;
        }
        else{
            
            // Extrapolation mode adjust here.
            if(true){
                return bullshit;
                //return null;
            }
            //print("\tresult: " + (searchResult-numTetras));
            if(!externalVolumes.get(searchResult-numTetras).containsPoint(p)){
                print("EV containsPoint error!");
                //print(""+ evsContainingPoint(p));
            }
            //int tetraIndex = externalVolumes.get(searchResult-numTetras).hullTetraIndex;
            return bullshit;//externalVolumes.get(searchResult-numTetras).tform;
            //return tetras.get(tetraIndex).tform;
        }
    }
    
        
    private PiecewiseAffineTransform3D(){
        //default constructor, used by copy constructor
        //don't use otherwise
    }
    
    public PiecewiseAffineTransform3D copy(){
        PiecewiseAffineTransform3D delNew = new PiecewiseAffineTransform3D();
        
        //deep copy points
        for(int i = 0; i < points.size(); i++){
            delNew.points.add(points.get(i).copy());
        }
        for(int i = 0; i < destPoints.size(); i++){
            delNew.destPoints.add(destPoints.get(i).copy());
        }
        
        //deep copy tetras
        for(int i = 0; i < tetras.size(); i++){
            //make a new tetra with the new points
            Point3D[] newTetraPoints = new Point3D[4];
            int[] newTetraPointIndices = new int[4];
            for(int j = 0; j < 4; j++){
                int pointIndex = tetras.get(i).pointIndices[j];
                newTetraPointIndices[j] = pointIndex;
                newTetraPoints[j] = delNew.points.get(pointIndex);
            }
            TessTetrahedron newTetra = new TessTetrahedron(newTetraPoints, newTetraPointIndices);
            
            //fill in tetra neighbors / faces,
            for(int j = 0; j < 4; j++){
                newTetra.externalVolumeIndices[j] = tetras.get(i).externalVolumeIndices[j];
                newTetra.neighborTetraIndices[j] = tetras.get(i).neighborTetraIndices[j];
            }
            
            //fill in tform
            newTetra.tform = new AffineTransform3D(tetras.get(i).tform.getTransform());
            
            //add the new tetra in
            delNew.tetras.add(newTetra);
        }
        
        //Add ExternalVolume bounding planes and neighbors
        //delNew.findExternalVolumes();
        
        return delNew;
    }
    
    private void makeDelaunayTetras(){
        /*
         * A very slow way to get a correct Delaunay tessellation.
         * O(N^4). This naive implementation is useful for testing
         * of better algorithms, as it is easily understandable and obviously correct.
         * Right now it's being used on its own; hopefully we'll replace it soon!
         */
        
        tetras.clear();
        for(int i = tetras.size()-1; i >= 0; i--){
            tetras.remove(i);
        }
        tetras = new ArrayList<TessTetrahedron>();
        
        for (int a = 0; a < points.size(); a++) {
            for (int b = a + 1; b < points.size(); b++) {
                for (int c = b + 1; c < points.size(); c++) {
                    for (int d = c + 1; d < points.size(); d++) {
                        Point3D[] tetraPoints = {points.get(a), points.get(b), points.get(c), points.get(d)};
                        int[] tetraPointIndices = {a, b, c, d};
                        TessTetrahedron t = new TessTetrahedron(tetraPoints, tetraPointIndices);

                        if (isDelaunayTetra(t)) {
                            tetras.add(t);
                        }
                    }
                }
            }
        }
    }
    
    private void makeDelaunayTetrasAndExtrapolate(Box3D imageBox){
        //start with a tessellation of the existing points
        makeDelaunayTetras();
        fillInNeighbors();
        makeTransforms();
        
        print("tetras at iter=0: " + tetras.size());
        
        Plane3D[] imageSurfaces = imageBox.surfaces;
        Point3D[] imageCorners = imageBox.corners;
        
        int iter = 1;
        
        //Adds points outside of the convex hull to extend the 
        //volume to the borders of the image
        boolean done = false;
        while(! done){
            print("iteration " + iter);
            StringBuilder sb = new StringBuilder();
            
            //need to add more points
            ArrayList<TessTetrahedron> hullTetras = getHullTetras();
            for(int i = 0; i < hullTetras.size(); i++){
                TessTetrahedron t = hullTetras.get(i);
                for(int j = 0; j < t.faces.length; j++){
                    if(t.neighborTetraIndices[j] == -1){
                        //this is a convex hull face
                        //get face points
                        Point3D[] facePoints = t.getFacePoints(j);
                        
                        //three possibilities:
                        //(1) This face already lies on the surface of the image. Do nothing.
                        //(2) The points defining this face lie on adjacent image surfaces.
                        //Make a corner point where the image surfaces meet.
                        //(3) The face lies somewhere inside the image volume. Make a new
                        //point on an image surface plane.
                        
                        //test if face is inside any surface (case 1)
                        boolean faceOnSurface = false;
                        for(int s = 0; s < imageSurfaces.length; s++){
                            //are all 3 points on any one of the surfaces?
                            if(imageSurfaces[s].isOnPlane(facePoints[0]) &&
                                    imageSurfaces[s].isOnPlane(facePoints[1]) &&
                                    imageSurfaces[s].isOnPlane(facePoints[2])){
                                faceOnSurface = true;
                                break;
                            }
                        }
                        if(faceOnSurface){
                            //no need to do anything with this face
                            continue;
                        }
                        
                        //test if the 3 face points are on adjacent planes (case 2)
                        //first, find what plane(s) each point is on.
                        //note that a point that is in a corner or intersection line will be on 
                        //multiple surfaces. 
                        Point3D newCornerPoint = null;
                        for(int s0 = 0; s0 < imageSurfaces.length; s0++){
                            if(! imageSurfaces[s0].isOnPlane(facePoints[0])){
                                continue;
                            }
                            for(int s1 = 0; s1 < imageSurfaces.length; s1++){
                                if(! imageSurfaces[s1].isOnPlane(facePoints[1])){
                                    continue;
                                }
                                for(int s2 = 0; s2 < imageSurfaces.length; s2++){
                                    if(! imageSurfaces[s2].isOnPlane(facePoints[2])){
                                        continue;
                                    }
                                    //so the three points are on planes s0,s1,s2.
                                    //If the planes are orthogonal, they will intersect at
                                    //a corner point.
                                    Point3D intersectionPoint = imageSurfaces[s0].intersectPlanes(imageSurfaces[s1], imageSurfaces[s2]);
                                    if(intersectionPoint != null){
                                        newCornerPoint = intersectionPoint;
                                    }
                                }
                            }
                        }
                        
                        if(newCornerPoint != null){
                            //case 2.
                            //add the new corner point to the point set
                            //first make sure corner is not already in set
                            boolean alreadyGotCorner = false;
                            for(int k = 0; k < points.size(); k++){
                                if(newCornerPoint.isExtremelyCloseTo(points.get(k))){
                                    alreadyGotCorner = true;
                                    break;
                                }
                            }
                            if(alreadyGotCorner){
                                continue;
                            }
                            
                            Matrix allSrcPts = Point3D.pointArrayToMatrix(points.toArray(new Point3D[]{}), true);
                            Matrix allDestPts = Point3D.pointArrayToMatrix(destPoints.toArray(new Point3D[]{}), true);
                            AffineTransform3D globalAffine = new AffineTransform3D(allSrcPts, allDestPts);

                            
                            print(">>>>>>>>>>>>>>>adding corner point! " + newCornerPoint.toString());
                            points.add(newCornerPoint);
                            //also add its corresponding dest point
                            //Matrix destPtMat = t.tform.applyToPoints(newCornerPoint.toMatrix(true));
                            //Point3D destPt = Point3D.matrixToPointArray(destPtMat)[0];
                            //destPoints.add(destPt);
                            
                            
                            Matrix destPtMat = globalAffine.applyToPoints(newCornerPoint.toMatrix(true));
                            Point3D destPt = Point3D.matrixToPointArray(destPtMat)[0];
                            destPoints.add(destPt);
                        }                        
                        else{
                            //case 3 - shoot a new point onto one of the image surfaces
                            
                            Point3D faceCenter = Point3D.getAverage(facePoints);
                            Point3D lineVector = t.faces[j].normal;
                            Ray3D ray = new Ray3D(faceCenter, lineVector);
                            
                            //The planes extend out to infinity and we
                            //only want to hit the plane closest along
                            //the ray. Find the intersection on the closest 
                            //surface plane.
                            double minDist = Double.MAX_VALUE;
                            int surfaceHit = -1;
                            for(int s = 0; s < imageSurfaces.length; s++){
                                Double dist = imageSurfaces[s].intersectRay(ray);
                                if(dist == null || dist <= 0)
                                    continue;
                                if(dist < minDist){
                                    minDist = dist;
                                    surfaceHit = s;
                                }
                            }
                            
                            
                            if(surfaceHit != -1){
                                Point3D intersectionPoint = lineVector.times(minDist).add(faceCenter);
                                
                                
                                Matrix allSrcPts = Point3D.pointArrayToMatrix(points.toArray(new Point3D[]{}), true);
                                Matrix allDestPts = Point3D.pointArrayToMatrix(destPoints.toArray(new Point3D[]{}), true);
                                AffineTransform3D globalAffine = new AffineTransform3D(allSrcPts, allDestPts);
                                
                                //add the new point pair to the point set
                                points.add(intersectionPoint);
                                //intersectionPoint is on source image -- what about dest?
                                //use the tetra's transform to determine where the point lands
                                //in the dest image volume.
                                
                                //Matrix destPtMat = t.tform.applyToPoints(intersectionPoint.toMatrix(true));
                                //Point3D destPt = Point3D.matrixToPointArray(destPtMat)[0];
                                

                                Matrix destPtMat = globalAffine.applyToPoints(intersectionPoint.toMatrix(true));
                                Point3D destPt = Point3D.matrixToPointArray(destPtMat)[0];
                                destPoints.add(destPt);
                            }
                        }
                    }
                }
            }
            
            sb.append("points:\n");
            for(int i = 0; i < points.size(); i++){
                String pointStr = points.get(i).toString() + "\n";
                sb.append(pointStr);
            }
            
            sb.append("\ndest points:\n");
            for(int i = 0; i < destPoints.size(); i++){
                String pointStr = destPoints.get(i).toString() + "\n";
                sb.append(pointStr);
            }
            //remake tessellation with new points
            print("Running Delaunay, iteration " + iter + "...");
            
            makeDelaunayTetras();
            fillInNeighbors();
            makeTransforms();
            
            sb.append("\ntetras at iter="+iter+": " + tetras.size());
            sb.append("\n");
            
            FileAndDirOperations.writeStringToFile(sb.toString(), "G:/OutImages/points_" + iter + ".txt");
            
            //check if done
            done = true;
            for(int i = 0; i < imageCorners.length; i++){
                boolean hasThisCorner = false;
                for(int j = 0; j < points.size(); j++){
                    if(points.get(j).isExtremelyCloseTo(imageCorners[i])){
                        hasThisCorner = true;
                        break;
                    }
                }
                if(! hasThisCorner){
                    print("Didn't have corner " + i);
                    done = false;
                    break;
                }
            }
            
            if(iter == 2){
                done = true;
            }
            
            if(done){
                print("extrapolation complete!");
            }
            iter++;
        }
        
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < tetras.size(); i++){
            TessTetrahedron t = tetras.get(i);
            sb.append("point indices " + t.pointIndices[0] + " " + t.pointIndices[1] + " " + t.pointIndices[2] + " " + t.pointIndices[3] + "\n");
            Matrix tform = t.tform.getTransform();
            for(int j = 0; j < tform.getRowDimension(); j++){
                for(int k = 0; k < tform.getColumnDimension(); k++){
                    sb.append(tform.get(j, k) + "  ");
                }
                sb.append("\n");
            }
            sb.append("\n");
        }
        try{ Thread.sleep(10000); } catch(Exception ex){}
        FileAndDirOperations.writeStringToFile(sb.toString(), "G:/OutImages/tetras.txt");
    }
    
    public ArrayList<TessTetrahedron> getHullTetras(){
        //returns all tetras that have at least one face on the convex hull
        ArrayList<TessTetrahedron> hullTetras = new ArrayList<TessTetrahedron>();
        for(int i = 0; i < tetras.size(); i++){
            TessTetrahedron t = tetras.get(i);
            for(int j = 0; j < t.faces.length; j++){
                if(t.neighborTetraIndices[j] == -1){
                    hullTetras.add(t);
                    break;
                }
            }
        }
        return hullTetras;
    }
    
    private void makeTransforms(){
        //define affine transforms that map each tetra in srcPoints to 
        //their corresponding destPoints
        
        for(int i = 0; i < tetras.size(); i++){
            int[] indices = tetras.get(i).pointIndices;
            Point3D[] srcPts = new Point3D[4];
            Point3D[] destPts = new Point3D[4];
            for(int j = 0; j < 4; j++){
                srcPts[j] = points.get(indices[j]);
                //destPts[j] = Point3D.matrixToPointArray(globalAffine.applyToPoints(destPoints.get(indices[j]).toMatrix(true)))[0];
                destPts[j] = destPoints.get(indices[j]);
            }
            Matrix srcMat = Point3D.pointArrayToMatrix(srcPts, true);
            Matrix destMat = Point3D.pointArrayToMatrix(destPts, true);
            try{
                AffineTransform3D tform = new AffineTransform3D(srcMat, destMat);
                //now undo global affine
                //tetras.get(i).tform = new AffineTransform3D(tform.getTransform().times(globalAffine.getInverseTransform()));
                tetras.get(i).tform = tform;
            }
            catch(Exception ex){
                tetras.get(i).tform = bullshit;
                ex.printStackTrace();
            }
        }
    }

    private void fillInNeighbors(){
        //match faces to find neighbors
        //also slow, but not as egregious as the above
        //easy to speed up by using a hashmap
        for (int i = 0; i < tetras.size(); i++) {
            for (int j = 0; j < tetras.size(); j++) {
                if(i==j)
                    continue;
                //for each pair of tetras, see if any of their faces match
                for(int iFace = 0; iFace < 4; iFace++){
                    for(int jFace = 0; jFace < 4; jFace++){
                        Plane3D fi = tetras.get(i).faces[iFace];
                        Plane3D fj = tetras.get(j).faces[jFace];
                        if(fi.matches(fj)){
                            tetras.get(i).neighborTetraIndices[iFace] = j;
                            tetras.get(j).neighborTetraIndices[jFace] = i;
                        }
                    }
                }
            }
        }
    }
            
    private void findExternalVolumes(){
        //find tetras with faces that are on the convex hull
        //make an ExternalVolume for each such face
        for (int i = 0; i < tetras.size(); i++) {
            for (int j = 0; j < tetras.get(i).neighborTetraIndices.length; j++) {
                if (tetras.get(i).neighborTetraIndices[j] == -1) {
                    //make an externalVolume connected to this face
                    ExternalVolume ev = new ExternalVolume(tetras.get(i).faces[j], tetras.get(i).points[j]);
                    int evIndex = externalVolumes.size();
                    tetras.get(i).externalVolumeIndices[j] = evIndex;
                    ev.hullTetraIndex = i;
                    externalVolumes.add(ev);
                }
            }
        }
        //Each externalVolume still needs to know where its three boundingPlanes
        //are and what its neighbors are.
        for(ExternalVolume ev: externalVolumes){
            TessTetrahedron evTetra = tetras.get(ev.hullTetraIndex);
            int evTetraFace = 0;
            for(int i = 0; i < evTetra.faces.length; i++){
                if(evTetra.faces[i].matches(ev.hullFace)){
                    evTetraFace = i;
                }
            }
            
            
            //Build a list of points from which we will derive the 
            //transform performed by this externalVolume.
            //It will include this tetrahedron's points and the points of 
            //its three neighbors on the convex hull. So at most 10 points.
            //(If it has fewer than three neighbors, there will be fewer points.)
            ArrayList<Point3D> tformSrcPoints = new ArrayList<Point3D>();
            ArrayList<Point3D> tformDestPoints = new ArrayList<Point3D>();
            
            //add evTetra's points to transform point set
            for(int ti = 0; ti < 4; ti++){
                tformSrcPoints.add(points.get(evTetra.pointIndices[ti]));
                tformDestPoints.add(destPoints.get(evTetra.pointIndices[ti]));
            }
            
            
            //For each edge on this face, there is one face
            //on some tetrahedron that is on the convex hull
            //that shares the edge. (Possibly the same tetrahedron, e.g.
            //if there's only one tetra in the tessellation.)
            
            int neighborNum = 0;
            for(int tc = 0; tc < evTetra.pointIndices.length; tc++){
                if(tc == evTetraFace){
                    continue;
                }
                
                //edge (ta,tb) contains the two remaining points in either order
                int ta = 0;
                while(ta == tc || ta == evTetraFace){
                    ta = (ta + 1) % 4;
                }
                int tb = 0;
                while(tb == ta || tb == tc || tb == evTetraFace){
                    tb = (tb + 1) % 4;
                }
                int[] edge = {evTetra.pointIndices[ta], evTetra.pointIndices[tb]};

                Point3D tFaceNormal = evTetra.faces[evTetraFace].normal;
                
                
                Point3D neighborFaceNormal;
                if(evTetra.neighborTetraIndices[tc] == -1){
                    //This tetra has both face ABD and ABC on the convex hull
                    //calculate the bounding plane normal from those two
                    //neighborWallNormal = evTetra.faces[evTetraFace].normal;
                    neighborFaceNormal = evTetra.faces[tc].normal;
                    
                    //neighboring volume is on this tetra
                    ev.neighbors[neighborNum] = evTetra.externalVolumeIndices[tc];
                }
                else{
                    //we need to find the other tetrahedron on
                    //the convex hull whose convex hull face contains
                    //edge {ta,tb}.
                    int uIndex = findTetraWithHullFaceContainingEdge(edge, ev.hullTetraIndex);
                    TessTetrahedron u = tetras.get(uIndex);
                    
                    //add u's points to transform
                    for(int ui = 0; ui < 4; ui++){
                        if(! tformSrcPoints.contains(points.get(u.pointIndices[ui]))){
                            tformSrcPoints.add(points.get(u.pointIndices[ui]));
                            tformDestPoints.add(destPoints.get(u.pointIndices[ui]));
                        }
                    }
                    
                    //found neighboring volume
                    int hullFace = u.getHullFaceContainingEdge(edge);
                    ev.neighbors[neighborNum] = u.externalVolumeIndices[hullFace];
                    
                    neighborFaceNormal = u.faces[hullFace].normal;
                }
                
                //vector along plane will be the sum of neighbor
                Point3D vectorAlongPlane = neighborFaceNormal.add(tFaceNormal);
                
                //fill in some points for the new bounding plane.
                //we know that ta and tb are on the bounding plane.
                Point3D[] planePts = new Point3D[3];
                planePts[0] = evTetra.points[ta];
                planePts[1] = evTetra.points[tb];
                
                Point3D avgPoint = evTetra.points[ta].add(evTetra.points[tb]);
                avgPoint.x = avgPoint.x / 2;
                avgPoint.y = avgPoint.y / 2;
                avgPoint.z = avgPoint.z / 2;
                
                planePts[2] = avgPoint.add(vectorAlongPlane);
                
                ev.boundingPlanes[neighborNum] = new Plane3D(planePts);
                
                //normal should point away from tc
                if(!ev.boundingPlanes[neighborNum].normalPointsAwayFrom(evTetra.points[tc])){
                    ev.boundingPlanes[neighborNum].flipNormal();
                }
                
                neighborNum++;
            }
            
            //set ev transform
            Matrix srcPtsMat = Point3D.pointArrayToMatrix(tformSrcPoints.toArray(new Point3D[]{}), true);
            Matrix destPtsMat = Point3D.pointArrayToMatrix(tformDestPoints.toArray(new Point3D[]{}), true);
            try{
                ev.tform = new AffineTransform3D(srcPtsMat, destPtsMat);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public int findTetraWithHullFaceContainingEdge(int[] edge, int tetraIndex){
        //given an edge e on the convex hull, there are a set of tetras
        //around that edge. Two such tetras will have a face containing the 
        //edge that sits on the hull. Starting at one, find the other.
        //Search will look like this:
        /*Edge is represented by a "."; we are looking at it end-on.
         *  \         /
         * hull      /
         *    \  1  /
         *     \   /  2
         *      \./__________
         *        \
         *         \  3
         * outside  \
         *           hull
         *            \
         * We start at tetra 1, then go to tetra 2, then arrive at tetra 3 and return it.
         */          
        int prevIndex = -1;
        TessTetrahedron u = tetras.get(tetraIndex); 
        
        //start searching neighbors of u
        while(true){
            for(int i = 0; i < 4; i++){
                if(u.neighborTetraIndices[i] == -1 || u.neighborTetraIndices[i] == prevIndex){
                    //never go backwards, dont run outside the hull either
                    continue;
                }
                TessTetrahedron neighborTetra = tetras.get(u.neighborTetraIndices[i]);
                if(ArrayUtils.arrayContains(neighborTetra.pointIndices, edge[0]) &&
                        ArrayUtils.arrayContains(neighborTetra.pointIndices, edge[1])){
                    //the tetra contains this edge. 
                    //We're going the right way. Step forwards.
                    prevIndex = tetraIndex;
                    tetraIndex = u.neighborTetraIndices[i];
                    u = tetras.get(tetraIndex);

                    //check if the tetra has the edge on a hull face.
                    for(int d = 0; d < 4; d++){
                        if(u.neighborTetraIndices[d] == -1){
                            //this face is on the hull, see if it contains the edge
                            int[] facePointIndices = u.getFacePointIndices(d);
                            int pointsMatched = 0;
                            for(int fp = 0; fp < facePointIndices.length; fp++){
                                if(facePointIndices[fp] == edge[0] || facePointIndices[fp] == edge[1]){
                                    pointsMatched++;
                                }
                            }
                            if(pointsMatched == 2){
                                return tetraIndex;
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isDelaunayTetra(TessTetrahedron t) {
        Sphere s = new Sphere(t.points);
        if(s.radius < Constants.EPSILON){
            return false;
        }
        for (int i = 0; i < points.size(); i++) {
            boolean isTetraPoint = false;
            for(int j = 0; j < t.pointIndices.length; j++){
                if(t.pointIndices[j] == i){
                    isTetraPoint = true;
                    break;
                }
            }
            if(isTetraPoint){
                continue;
            }
            if (s.containsPoint(points.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    //test functions from here down.
    
    public ArrayList<Integer> tetrasContainingPoint(Point3D p){
        //brute force search over tetras
        //to find which one contains a given point
        //used only in testing
        ArrayList<Integer> hits = new ArrayList<Integer>();
        for(int i = 0; i < tetras.size(); i++){
            if(tetras.get(i).containsPoint(p)){
                hits.add(i);
            }
        }
        return hits;
        
    }
    
    public ArrayList<Integer> evsContainingPoint(Point3D p){
        //brute force search over externalVolumes
        //to find which one contains a given point
        //used only in testing
        ArrayList<Integer> hits = new ArrayList<Integer>();
        for(int i = 0; i < externalVolumes.size(); i++){
            if(externalVolumes.get(i).containsPoint(p)){
                hits.add(i);
            }
        }
        return hits;
    }
    
    public void printConnectionGraph(){
        for(int i = 0; i < tetras.size(); i++){
            print("tetra " + i);
            String s = "\t neighboring tetras:";
            for(int j = 0; j < 4; j++){
                if(tetras.get(i).neighborTetraIndices[j] != -1){
                    s += " " + tetras.get(i).neighborTetraIndices[j];
                }
            }
            print(s);
            s = "\t neighboring evs:";
            for(int j = 0; j < 4; j++){
                if(tetras.get(i).externalVolumeIndices[j] != -1){
                    s += " " + tetras.get(i).externalVolumeIndices[j];
                }
            }
            print(s);
        }
        
        for(int i = 0; i < externalVolumes.size(); i++){
            print("external volume " + i);
            String s = "\t neighboring tetra:" + externalVolumes.get(i).hullTetraIndex;
            print(s);
            s = "\t neighboring evs:";
            for(int j = 0; j < 3; j++){
                s += " " + externalVolumes.get(i).neighbors[j];
            }
            print(s);
        }
        
    }
    
    public void validateEvNormals(){
        //for each externalVolume normal, make sure there's a normal that 
        //mirrors it in another externalVolume
        for(int i = 0; i < externalVolumes.size(); i++){
            ExternalVolume ev = externalVolumes.get(i);
            for(int j = 0; j < 3; j++){
                ExternalVolume evNeighbor = externalVolumes.get(ev.neighbors[j]);
                Point3D myNormal = ev.boundingPlanes[j].normal;
                for(int k = 0; k < 3; k++){
                    if(evNeighbor.neighbors[k] == i){
                        Point3D neighborNormal = evNeighbor.boundingPlanes[k].normal;
                        if(myNormal.add(neighborNormal).getMagnitude() > Constants.EPSILON){
                            print("ERROR FOUND!");
                            print("my normal: " + myNormal);
                            print("neighbor normal: " + neighborNormal);
                            print("");
                        }
                    }
                }
            }
            //also check normals of neighbor tetras
            Point3D myNormal = ev.hullFace.normal;
            TessTetrahedron t = tetras.get(ev.hullTetraIndex);
            for(int j = 0; j < 4; j++){
                if(t.externalVolumeIndices[j] == i){
                    Point3D neighborNormal = t.faces[j].normal;
                    if(myNormal.add(neighborNormal).getMagnitude() > Constants.EPSILON){
                        print("ERROR FOUND!");
                        print("my normal: " + myNormal);
                        print("neighbor normal: " + neighborNormal);
                        print("");
                    }
                }
            }
        }
    }
    
    public void validateTetraNormals(){
        //for each tetra normal, make sure there's a normal that 
        //mirrors it in another tetra
        //anything involving externalVolumes is checked elsewhere
        //so don't worry about those.
        for(int i = 0; i < tetras.size(); i++){
            TessTetrahedron t = tetras.get(i);
            for(int j = 0; j < t.neighborTetraIndices.length; j++){
                if(t.neighborTetraIndices[j] == -1){
                    continue;
                }
                Point3D myNormal = t.faces[j].normal;
                TessTetrahedron u = tetras.get(t.neighborTetraIndices[j]);
                for(int k = 0; k < u.faces.length; k++){
                    if(u.neighborTetraIndices[k] == i){
                        Point3D neighborNormal = u.faces[k].normal;
                        if(myNormal.add(neighborNormal).getMagnitude() > Constants.EPSILON){
                            print("ERROR FOUND!");
                            print("my normal: " + myNormal);
                            print("neighbor normal: " + neighborNormal);
                            print("");
                        }
                    }
                }
            }
        }
    }
    
    public boolean isInCachedSpot(Point3D p){
        int numTetras = tetras.size();
        if(lastPointSearchResult < numTetras){
            //we're at a tetrahedron
            //look through its neighbors to figure out where we go next            
            return tetras.get(lastPointSearchResult).containsPoint(p);
        }
        else{
            //we're at an externalVolume
            //look through its neighbors to figure out where we go next
            int evIndex = lastPointSearchResult - numTetras;
            return externalVolumes.get(evIndex).containsPoint(p);
        }

    } 
    
    public static void print(String s){
        System.out.println(s);
    }
}
