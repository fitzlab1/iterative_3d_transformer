/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author walkert
 */
public class ExecTime {
    long startTime;
    
    public void tic(){
        startTime = System.nanoTime();
    }
    
    public long toc(){
        return System.nanoTime() - startTime;
    }
}
