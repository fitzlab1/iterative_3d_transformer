package transforms;

import geometry.Point3D;
import geometry.Edge3D;
import geometry.Plane3D;
import geometry.Point3D;
import geometry.Tetrahedron;
import java.util.ArrayList;


public class TessTetrahedron extends Tetrahedron {
    //Each TessTetrahedron is assumed to be part of a tessellation
    //that was calculated from a specific point set. The TessTetrahedron variables
    //only have meaning in the context of that tessellation.
    
    public int[] pointIndices; // there will be 4, ordered: {a, b, c, d}.
    public int[] neighborTetraIndices; //indices of the 4 neighbor tetras. 
    //if any neighbor tetra is missing, indicated by a -1 value, this tetra
    //is on the convex hull.
    //neighborIndices order is the same as normals: {bcd, acd, abd, abc}.
    
    public int[] externalVolumeIndices; //if face is on convex hull, this is the
    //index for the neighboring ExternalVolume. If not, -1.
    
    public boolean visited = false; //used in point searching
    
    public AffineTransform3D tform; //used in piecewise affine image transformation
    
    public TessTetrahedron(Point3D[] points_, int[] pointIndices_){
        super(points_);
        pointIndices = pointIndices_;
        points = points_;
        
        neighborTetraIndices = new int[4];
        externalVolumeIndices = new int[4];
        for(int i = 0; i < 4; i++){
            //initially no neighbors filled in. Delaunay class does this.
            neighborTetraIndices[i] = -1; 
            externalVolumeIndices[i] = -1;
        }
        
    }
    
    public int[] getFacePointIndices(int f){
        //return point indices for some face
        int[] face = new int[3];
        int index = 0;
        for(int i = 0; i < 4; i++){
            if(i==f){
                continue;
            }
            face[index] = pointIndices[i];
            index++;
        }
        return face;
    }
    
    public Point3D[] getFacePoints(int f){
        //return points for some face
        Point3D[] face = new Point3D[3];
        int index = 0;
        for(int i = 0; i < 4; i++){
            if(i==f){
                continue;
            }
            face[index] = points[i];
            index++;
        }
        return face;
    }
    
    public int[] findPoint(Point3D p, int myIndex, int numTetras){
        //returns indices of neighboring tetras that might contain p.
        //considers neighbors on all 4 faces of tetrahedron, so returns 4 values.
        //value = neighborIndex indicates that we should look towards that neighbor.
        //value = myIndex indicates that neighbor cannot contain p.
        //so, if all 4 values returned are myIndex, p is in this tetra.
        int[] neighborsTowardsP = new int[4];
        
        for(int i = 0; i < 4; i++){
            if(faces[i].normalPointsAwayFrom(p) || faces[i].isOnPlane(p)){ 
                //p is on the inside of this face; neighbor
                //cannot contain p
                neighborsTowardsP[i] = myIndex;    
            }
            else{
                //neighbor may contain p
                if(neighborTetraIndices[i] == -1){
                    //neighbor is an externalVolume
                    //add numTetras to it to tell the search algorithm
                    //that it's not a tetra
                    neighborsTowardsP[i] = externalVolumeIndices[i] + numTetras;
                }
                else{
                    //neighbor is a tetrahedron
                    neighborsTowardsP[i] = neighborTetraIndices[i];
                }        
            }
        }
        return neighborsTowardsP;
    }
    
    public int getNonHullFaceContainingEdge(int[] edge){
        for(int d = 0; d < points.length; d++){
            if(neighborTetraIndices[d] != -1){
                int[] facePointIndices = getFacePointIndices(d);
                int edgePointsMatched = 0;
                for(int i = 0; i < facePointIndices.length; i++){
                    if(facePointIndices[i] == edge[0] || facePointIndices[i] == edge[1]){
                        edgePointsMatched++;
                    }
                }
                if(edgePointsMatched == 2){
                    return d;
                }
            }
        }
        //no such face exists
        return -1;
    }
    public int getHullFaceContainingEdge(int[] edge){
        for(int d = 0; d < points.length; d++){
            if(neighborTetraIndices[d] == -1){
                int[] facePointIndices = getFacePointIndices(d);
                int edgePointsMatched = 0;
                for(int i = 0; i < facePointIndices.length; i++){
                    if(facePointIndices[i] == edge[0] || facePointIndices[i] == edge[1]){
                        edgePointsMatched++;
                    }
                }
                if(edgePointsMatched == 2){
                    return d;
                }
            }
        }
        //no such face exists
        return -1;
    }
    
    
    public static void print(String s){
        System.out.println(s);
    }
}
