<html>

<body>

<div style="width:900px;">

<p><h2>Iterative 3D Transformer</h2></p>

<p>Iterative 3D Transformer is an ImageJ plugin for doing 3D affine image transformation using control points. It is commonly used for registering <i>in vivo</i> 2-photon images to histological images.</p>

<p></p>

<p><h3>Download</h3></p>

<a href="files/Iterative_3D_Transformer.jar">Iterative 3D Transformer</a>

<p>Place the Iterative_3D_Transformer.jar file in your ImageJ plugins folder. Restart ImageJ, and you can run Iterative 3D Transformer from the plugins menu.</p>

<br />
<p><h3>Transforming Example Data</h3></p>

<p>To get familiar with the program, it's worth running on example data first to see the output.</p>

<p><a href="files/example_dataset.zip">Download the example dataset</a></p>

<p>Extract the files from the example dataset. Drag the folders "Histological" and "In Vivo" onto your ImageJ window and open them as stacks.</p>

<p>Start the Iterative 3D Transformer plugin. Click "Refresh Windows". Set the Source Points Image to "Histological" and the Destination Points Image to "In Vivo". Leave the Source Transformed field blank.</p>

<p>Click on "Load Points", and open the "control-points.txt" file from the Control Points directory. Iterative 3D Transformer should look like this:</p>

<p><img src="files/after_load_points.png" /></p>

<p>Now, change to the Transform tab. Set the Source image to "Histological" and the Destination image to "In Vivo". Click on Run Transform, and wait a few minutes for it to finish. A new window will pop up containing the transformed histological data. While the topmost Z position will be black, you can move in Z to see the transformed section.</p>

<p><img src="files/make_composite.png"></p>

<p>To visualize how the transformed histological data matches up with the in vivo data, create a composite image. From the ImageJ menu, use Image -> Color -> Merge Channels. Set the Histological data to red and the In Vivo data to green. Make sure the other fields are blank. Choose "Keep Source Images". Click OK.</p>

<p>Move through Z in the composite image. You will see the match between the histological and in-vivo data.</p>

<img src="files/composite_results.png">

<p>Looks good! The yellow areas in the composite indicate where both the red and green channels match with high intensity, since red + green = yellow. You can toggle the green channel on and off using Image -> Color -> Channels Tool to confirm the registration.</p>

<p>For this example, we loaded existing point correspondences to register the in vivo data to the histological. In practice, you'll need to specify correspondences on your own.</p>

<br />
<p><h3>Step 1: 2D Transform</h3></p>

<p>Having seen the results of a good transform, now it's time to make your own. Close ImageJ and reopen it to start fresh. Open the In_Vivo and Histological datasets again. Start the Iterative 3D Transformer plugin and click "Refresh Window List". As before, set the Source Image to "Histological" and the Target to "In Vivo".</p>

<p>Our goal is to get cells to line up, like in the above image. But In most cases, finding corresponding cells between the two datasets is too hard to do by eye at first. For that reason, we adopt an iterative approach. First, we'll define some 2D correspondences, then we'll get a rough 3D transformation, then we'll refine that transformation to make it very accurate.</p>

<p>The first step is to get a good 2D transform based on the radial blood vessels. Move to Z=138 on the In Vivo stack, and Z=18 on the Histological stack. You'll see a pattern of seven radial blood vessels that match between the two datasets. We'll use those to get our initial transform.</p>

<p>Select the Point tool from the ImageJ toolbar. Hold down Shift and click on the radial blood vessels one at a time, in the numbered order as shown below. When you add all seven points on an image, press the "T" key on your keyboard to add the points to the ROI Manager.</p>

<p><img src="files/radials.png"></p>

<p>Next, click "Refresh Points List" in Iterative 3D Transformer. A list of seven points will appear in the Source and Destination tables. Change to the Transform tab, select "Histological" as the source and "In Vivo" as the Destination. Then Run Transform.</p>

<p><img src="files/2d_transform_result.png"></p>

<p><h3>Step 2: Rough 3D Transform</h3></p>

<p>Scroll through Z in the new Source Transformed image. Observe changes in the radial blood vessels as you move through Z. Corresponding neurons may be visible as well. Using any correspondences we can feel confident in, we'll develop a rough 3D transform.</p>

<p>Note that in this dataset, the Histological data is inverted in Z relative to the In Vivo. This has a 50% chance of happening, since brain slices float freely during preparation and so can be mounted either side up on the slide.</p>

<p>We no longer need the points from our 2D transform, since we're working towards 3D now. Select all the points in the ROI Manager (Ctrl+A), and select Delete. In Iterative 3D Transformer, click "Refresh Points List" to clear that as well.</p>

<p>Scrolling up and down in Z on the Source Transformed image will allow you to spot some 3D correspondences. Using ImageJ's Point tool, click on correspondences in each image and then press T to add each point to the ROI manager. Finding 3D correspondences is a skill: you get better and faster at it over time. Here are several correspondences in this dataset:</p>

<p><img src="files/step2_4.png"></p>
<p><img src="files/step2_5.png"></p>
<p><img src="files/step2_6.png"></p>
<p><img src="files/step2_7.png"></p>
<p><img src="files/step2_1.png"></p>
<p><img src="files/step2_2.png"></p>
<p><img src="files/step2_3.png"></p>
<p></p>

<p>Those seven correspondences are spread throughout the 3D volume, so they make the basis for a good transform. Typically 6 to 10 correspondences are enough as long as they're not all in the same plane. 

<p>Click "Refresh Points List" and then run the transform. The result will look something like:</p>

<p><img src="files/rough_composite.png" /></p>


<p><h3>Step 3: Fine 3D Transform</h3></p>

<p>You may the previous "Source Transformed" window, it's no longer needed. 

<p>The rough transform may be good enough depending on your purpose. There are small inaccuracies such as the one circled in the image above. To correct these, add more points in the areas where the transform is the most inaccurate. You can also remove a few of the worst correspondences using the ROI manager. </p>

<p>Typically no more than 20 correspondences are needed to get a perfect 3D transform. Note that you may be able to get multiple good correspondences out of a single cell or blood vessel by following its path as you move in Z.</p>

<p>When you're done, the transform should resemble the one we got by loading the example point set at the beginning of this tutorial.</p>

<p><h3>About:</h3></p>
<p>Iterative 3D Transformer v1.0 released July 8, 2016.</p>

<p>Iterative 3D Transformer is known to work with the latest version of Fiji at time of writing. If developments in Fiji break compatibility, try using it with Fiji 1.8. Download Fiji 1.8 for your OS: <a href="files/fiji-win64-20130715.zip">Windows</a> | <a href="files/fiji-linux64-20130715.tar.gz">Linux</a> | <a href="files/fiji-macosx-20130715.dmg">OSX</a></p>

<p>Created by <a href="http://www.maxplanckflorida.org/fitzpatricklab/theo-walker.html">Theo Walker</a> (<a href="mailto:theo.walker@mpfi.org">Email</a>).</p>

</div>
</body>
</html>