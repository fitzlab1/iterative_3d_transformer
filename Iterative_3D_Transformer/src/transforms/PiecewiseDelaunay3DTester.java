
package transforms;

import geometry.Point3D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import utils.ExecTime;

public class PiecewiseDelaunay3DTester {
    //uses several inputs to test the delaunay class
    PiecewiseAffineTransform3D del;
    
    public PiecewiseDelaunay3DTester(int numDelaunayPoints){
        //make a random tessellation the given size
        del = makeRandomPointDelaunay(numDelaunayPoints);
    }
    
    public PiecewiseDelaunay3DTester(){
        //make a simple two-tetrahedron case
        ArrayList<Point3D> srcPts = new ArrayList<Point3D>();
        ArrayList<Point3D> destPts = new ArrayList<Point3D>();
    
        srcPts.add(new Point3D(new double[] {0,-2,-1}));
        srcPts.add(new Point3D(new double[] {0,2,-1}));
        srcPts.add(new Point3D(new double[] {0,0,2}));
        srcPts.add(new Point3D(new double[] {-3,0,0}));
        srcPts.add(new Point3D(new double[] {3,0,0}));
        
        //tform should be identity matrix
        destPts.add(new Point3D(new double[] {0,-2,-1}));
        destPts.add(new Point3D(new double[] {0,2,-1}));
        destPts.add(new Point3D(new double[] {0,0,2}));
        destPts.add(new Point3D(new double[] {-3,0,0}));
        destPts.add(new Point3D(new double[] {3,0,0}));
        
        //del = new PiecewiseAffineTransform3D(srcPts, destPts);
        del = null;
        
        print("\nconnections:");
        del.printConnectionGraph();
        
        print("\tetras:");
        for(int i = 0; i < del.tetras.size(); i++){
            print("tetra " + i);
            for(int j = 0; j < 4; j++){
                print(del.tetras.get(i).faces[j].normal.toString());
            }
        }

        print("\nevs:");
        for(int i = 0; i < del.externalVolumes.size(); i++){
            print("ev " + i);
            for(int j = 0; j < 3; j++){
                print(del.externalVolumes.get(i).boundingPlanes[j].normal.toString());
            }
            print(del.externalVolumes.get(i).hullFace.normal.toString());
        }
        
        print("validating normals");
        del.validateEvNormals();
        del.validateTetraNormals();
    }

    public void testOrigin(){
        //Prints the identity matrix at the origin
        AffineTransform3D tform = del.getTransform(new Point3D(new double[] {0,0,0}));
        tform.getTransform().print(8, 4);
    }
    
    public static PiecewiseAffineTransform3D makeRandomPointDelaunay(int numDelaunayPoints){
        //generate and test a large number of random points
        ArrayList<Point3D> srcPts = new ArrayList<Point3D>();
        ArrayList<Point3D> destPts = new ArrayList<Point3D>();
        
        for(int i = 0; i < numDelaunayPoints; i++){
            double[] srcPt = new double[3];
            double[] destPt = new double[3];
            
            for(int j = 0; j < 3; j++){
                srcPt[j] = Math.random() * 100 - 50;
                destPt[j] = srcPt[j] * 2; //stretch dest points by 2
            }
            srcPts.add(new Point3D(srcPt));
            destPts.add(new Point3D(destPt));
        }
        
        ExecTime et = new ExecTime();
        et.tic();
        //PiecewiseAffineTransform3D del = new PiecewiseAffineTransform3D(srcPts, destPts);
        PiecewiseAffineTransform3D del = null;
        double millis = et.toc() / 1000000;
        print("Delaunay generation time for " + numDelaunayPoints + " points: " + millis + " ms.");
        return del;
    }
    
    
    public void testCornerPoints(PiecewiseAffineTransform3D del){
        //test function
        //find the tetras and EVs that contain the corner points
        print("");
        print("Delaunay corner points test:");
        for(int i = 0; i < del.points.size(); i++){
            ArrayList<Integer> tetraHits = del.tetrasContainingPoint(del.points.get(i));
            print("tetra hits for point " + i + ": " + tetraHits);
            ArrayList<Integer> evHits = del.evsContainingPoint(del.points.get(i));
            print("ev hits for point " + i + ": " + evHits + "\n");
        }
    }
    
    public void testPointRange(int minX, int maxX, int minY, int maxY, int minZ, int maxZ){
        
        print("");
        print("Connection graph:");
        del.printConnectionGraph();
        
        print("");
        print("Validating normals\n");
        del.validateEvNormals();
        del.validateTetraNormals();
        
        ArrayList<Point3D> pointsToFind = new ArrayList<Point3D>();
        for(int x = minX; x < maxX; x++){
            for(int y = minY; y < maxY; y++){
                for(int z = minZ; z < maxZ; z++){
                    pointsToFind.add(new Point3D(new double[]{x,y,z}));
                }
            }    
        }
        
        Point3D[] pointsToFindArray = pointsToFind.toArray(new Point3D[]{});
        print("delaunay points:");
        for(int i = 0; i < del.points.size(); i++){
            print("" + del.points.get(i));
        }
        print("finding points...");
        
        ExecTime et = new ExecTime();
        et.tic();
        int cacheHits = 0;
        for(int i = 0; i < pointsToFindArray.length; i++){
            if(del.isInCachedSpot(pointsToFindArray[i])){
                cacheHits++;
            }
            del.getTransform(pointsToFindArray[i]);
            
            //ArrayList<Integer> pointInTetras = del.tetrasContainingPoint(pointsToFindArray[i]);
            //ArrayList<Integer> pointInEvs = del.evsContainingPoint(pointsToFindArray[i]);
            //print("point found in " + pointInTetras + " tetras and " + pointInEvs + " evs.");
        }
        
        
        double millis = et.toc() / 1000000;
        print("cache hits: " + cacheHits);
        print("Seek time for " + pointsToFindArray.length + " points: " + millis + " ms.");
    }
    
    public void testRandomPoints(){
        print("");
        print("Connection graph:");
        del.printConnectionGraph();
        
        print("");
        print("Point location speed test:");
        //go find a bunch of points
        //int numPointsToFind = 50*1024*1024*9;
        int numPointsToFind = 1024;
        ArrayList<Point3D> pointsToFind = new ArrayList<Point3D>();
        print("Generating points...");
        for(int i = 0; i < numPointsToFind; i++){
            double[] findPt = new double[3];
            for(int j = 0; j < 3; j++){
                findPt[j] = Math.random() * 100 - 50;
            }
            pointsToFind.add(new Point3D(findPt));
        }
        
        print("");
        print("Validating normals");
        del.validateEvNormals();
        
        Point3D[] pointsToFindArray = pointsToFind.toArray(new Point3D[]{});
        print("finding points...");
        ExecTime et = new ExecTime();
        et.tic();
        int cacheHits = 0;
        for(int i = 0; i < numPointsToFind; i++){
            if(del.isInCachedSpot(pointsToFindArray[i])){
                cacheHits++;
            }
            del.getTransform(pointsToFindArray[i]);
            
            //ArrayList<Integer> pointInTetras = del.tetrasContainingPoint(pointsToFindArray[i]);
            //ArrayList<Integer> pointInEvs = del.evsContainingPoint(pointsToFindArray[i]);
            //print("point found in " + pointInTetras + " tetras and " + pointInEvs + " evs.");
        }
        double millis = et.toc() / 1000000;
        print("cache hits: " + cacheHits);
        print("Seek time for " + numPointsToFind + " points: " + millis + " ms.");
    }
    
    
    public static void print(String s){
        System.out.println(s);
    }
}
