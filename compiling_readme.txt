Get IntelliJ Idea (Java IDE) -- community edition is free.

Open the Iterative_3D_Transformer subdirectory as a project.

You can then Build -> Make Project to compile, or Run it.

Build -> Build Artifacts will produce a .jar file of the Iterative_3D_Transformer source.

That .jar file will not include the libraries, and ImageJ needs the libraries in order to run the plugin.

You will need to produce a fat jar that includes the libraries in the lib directory except for ij.jar -- leave that one out.

JarSplice is a good tool for producing fat jars:
http://ninjacave.com/jarsplice

To make the fat jar, run jarsplice and:
- Add all jars (StackStitcher.jar + lib jars except ij.jar)
- No natives
- set the main class to: confocaltools.ConfocalToolsApp
- then create.


Why not put ij.jar in? That jar contains ImageJ's own libraries. Putting ImageJ's own jar into an ImageJ plugin adds a second copy of ImageJ's lib to its own path and makes ImageJ horribly confused. 

