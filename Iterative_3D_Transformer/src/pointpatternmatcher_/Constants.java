/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pointpatternmatcher_;

/**
 *
 * @author walkert
 */
public class Constants {
    public static final int FLIP_NONE = 0;
    public static final int FLIP_HORIZONTAL = 1;
    public static final int FLIP_VERTICAL = 2;
    public static final int NUM_THREADS = 8;
    
    //We need a small number to provide robustness against rounding errors
    //and to fix coplanar points in tessellation. 
    //Since all our points are going to be integers (pixel locations),
    //epsilon need not be near machine epsilon; it can be pretty big.
    //Smallest representable double is 2^-1074, or about 10^-100. 
    //So this epsilon gives us plenty of wiggle room; we can multiply a bunch
    //of them together without hitting machine epsilon, and it's small enough
    //that it will never mess with the integer-sized pixel location math.
    public static final double EPSILON = Math.pow(10, -6); 
}
