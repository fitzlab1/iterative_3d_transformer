/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import Jama.Matrix;
import pointpatternmatcher_.Constants;
import utils.ArrayUtils;

/**
 *
 * @author walkert
 */
public class Plane3D {
    public Point3D[] points = null;
    public Edge3D[] edges = null;
    public Point3D normal = null;

    public Plane3D(){
        points = new Point3D[3];
        normal = new Point3D();
        edges = new Edge3D[3];
    }
    
    public Plane3D(Point3D[] points_){        
        points = points_;
        calcEdges();
        calcNormal();        
    }
    
    public void calcEdges(){
        edges = new Edge3D[3];
        
        int e = 0; //edge index
        for(int f = 0; f < points.length; f++){
            for(int g = f+1; g < points.length; g++){
                edges[e] = new Edge3D(new Point3D[] {points[f], points[g]});
                e++;
            }
        }
    }
    
    public void calcNormal(){
        //make 2 vectors from the 3-point plane
        //cross 'em to get a normal
        Point3D vecA = points[1].subtract(points[0]);
        Point3D vecB = points[2].subtract(points[0]);
        normal = vecB.computeCrossProduct(vecA).toUnitVector();
    }
    
    public boolean isOnPlane(Point3D p){
        //is the point on this plane?
        //then adding / subtracting the normal vector from any one of the 3 points
        //will result in being the same distance from p.
        //We only need to check if it's coplanar to one of the 3 points in the 
        //plane because the 3 points are coplanar.
        Point3D normalAdded = points[0].add(normal); 
        Point3D normalSubtracted = points[0].subtract(normal);
        
        //pure equality is not robust against rounding errors, so we don't do this:
        //return p.getDistance(normalAdded) == p.getDistance(normalSubtracted);

        //we do this instead:
        if(Math.abs(p.getDistance(normalAdded) - p.getDistance(normalSubtracted)) < Constants.EPSILON)
            return true;
        else
            return false;
    }
    
    public boolean normalPointsAwayFrom(Point3D p){
        //does the normal vector direct away from the given point?
        Point3D normalAdded = points[0].add(normal); 
        Point3D normalSubtracted = points[0].subtract(normal);
        return p.getDistance(normalAdded) > p.getDistance(normalSubtracted);
    }
   
    public double angleBetween(Plane3D q){
        //it's just the angle between the normal vectors
        return normal.angleBetween(q.normal);
    }
    
    public void flipNormal(){
        //Points the normal in the opposite direction
        normal.x = -normal.x;
        normal.y = -normal.y;
        normal.z = -normal.z;
    }
    
    public Double intersectRay(Ray3D ray){
        //given an origin point and a vector (a ray)
        //find where the vector hits the plane
        //returns a distance d from p to the plane
        //returns null if the ray will never hit the plane, i.e. if the
        //ray is parallel to the plane.
        //Returns a negtive number if the plane is behind the ray, i.e.
        //the ray points away from the plane.
        
        double numerator = (points[0].subtract(ray.point)).dot(normal);
        double denominator = ray.vector.dot(normal);
        
        if(denominator < Constants.EPSILON){
            return null;
        }
        else{
            return new Double(numerator/denominator);
        }
        
    }
    
    
    public Point3D intersectPlanes(Plane3D p, Plane3D q){
        //finds the point at the intersection of three planes
        //returns null if no such point exists, e.g. some planes are parallel
        
        Matrix n1n2n3 = new Matrix(new double[][]{
            {normal.x, p.normal.x, q.normal.x},
            {normal.y, p.normal.y, q.normal.y},
            {normal.z, p.normal.z, q.normal.z}
        });
        
        double denominator = n1n2n3.det();
        if(denominator < Constants.EPSILON){
            //two or more of the planes are parallel; no solution possible
            return null;
        }
        
        Point3D p1 = (p.normal.computeCrossProduct(q.normal)).times(points[0].dot(normal));
        Point3D p2 = (q.normal.computeCrossProduct(normal)).times(p.points[0].dot(p.normal));
        Point3D p3 = (normal.computeCrossProduct(p.normal)).times(q.points[0].dot(q.normal));
        
        Point3D point = p1.add(p2.add(p3));
        return point.times(1.0/denominator);                
    }
    
    public boolean matches(Plane3D p){
        return ArrayUtils.arrayMatches(points, p.points);
    }
    
    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Plane3D))
            return false;
        Plane3D p = (Plane3D) o;
        
        for(int i = 0 ; i < points.length; i++){
            if(! points[i].equals(p.points[i]))
                return false;
        }
        return true;
    }
    
    @Override public int hashCode(){
        //hashcodes are commutative: equal planes will hash to the same spot.
        //vertices are in arbitrary order.
        //Allows for a quick hash-search to find matching faces.
        int result = 0;
        for(int i = 0 ; i < points.length; i++){
            result += points[i].hashCode();
        }
        return result;
    }
}
