package utils;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import geometry.Point3D;
import java.util.ArrayList;
import pointpatternmatcher_.Constants;

/*
 * A collection of static methods for calculating properties of matrices
 */
public class MatrixMethods {

    public static boolean isTransform2D(ArrayList<Point3D> srcPtsArrayList, ArrayList<Point3D> destPtsArrayList){
        boolean is2D = true;
        double zSrc = srcPtsArrayList.get(0).z;
        double zDest = destPtsArrayList.get(0).z;
        for(int i = 1; i < srcPtsArrayList.size(); i++){
            if(Math.abs(srcPtsArrayList.get(i).z - zSrc) > 0.1){
                is2D = false;
            }
        }
        for(int i = 1; i < destPtsArrayList.size(); i++){
            if(Math.abs(destPtsArrayList.get(i).z-zDest) > 0.1){
                is2D = false;
            }
        }
        return is2D;
    }
    
    public static double getResidual(Matrix p, Matrix q){
        
        double residual = 0;
        for(int i = 0; i < p.getRowDimension(); i++){
            double pointDifference = 0;
            for(int j = 0; j < p.getColumnDimension(); j++){
                double d = p.get(i, j) - q.get(i, j);
                pointDifference = pointDifference + Math.pow(d, 2);
            }
            residual = residual + Math.sqrt(pointDifference);
        }
        
        return residual / p.getRowDimension();
    }
    
    public static Matrix padWithOnes(Matrix m){
        //pads a matrix with ones on its right side
        //handy for generating valid inputs for making transforms or
        //applying them to points
        double[][] mPad = new double[m.getRowDimension()][m.getColumnDimension()+1];
        for(int i = 0; i < m.getRowDimension(); i++){
            for(int j = 0; j < m.getColumnDimension(); j++){
                mPad[i][j] = m.get(i,j);
            }
            mPad[i][m.getColumnDimension()] = 1;
        }
        return new Matrix(mPad);
    }
    
    
    //this function may be useful in future but right now is just silly
    private void calculateProperties(Matrix transform){
        //call this at the end of any constructors
        //generates the properties of the transform
        Matrix inverseTransform = transform.inverse();
        
        SingularValueDecomposition svd = transform.getMatrix(0,1,0,1).svd();
        
        double[] scaleFactors = svd.getSingularValues();
        double scaleFactor1 = scaleFactors[0];
        double scaleFactor2 = scaleFactors[1];
        
        //Find the angle of rotation 
        //The SVD of an affine transform decomposes it into a 
        //rotation (U), a scaling (S), and another rotation (V').
        //Multiplying the rotation matrices together will get the overall
        //rotation performed. 
        Matrix rotator = svd.getU().times(svd.getV().transpose());

        //A nice way to interpret this rotation matrix is as the result of
        //multiplying two unit vectors [1 0] and [0 1] by the rotation.
        //Since these unit vectors form an identity matrix, this is the same
        //as the rotator matrix itself, i.e.:
        //rotator = rotator * identityMatrix.
        //Consider, then, the two columns of "rotator" as being two vectors
        //that are the result of applying the rotator to the identity matrix.
        //Where do those vectors point? That will tell us our angle of rotation.
        
        //determine theta based on the angle from the x axis for vec 1
        double angle1 = Math.acos(rotator.get(0,0))*180/Math.PI;
        //determine theta based on the angle from the y axis for vec 2
        //need both pieces of information to discriminate flip types.
        double angle2 = Math.acos(rotator.get(1,1))*180/Math.PI;
        
        //acos will only return a positive (Q1) result, so we need the quadrant
        //to determine whether the angle is negative or positive
        if(rotator.get(0,1) < 0){
            //y-coordinate is negative; we are in Q3 or Q4, so angle is negative
            angle1 = -angle1;
        }
        if(rotator.get(1,0) > 0){
            //x-coordinate is positive; we are in Q1 or Q4, so angle is negative
            angle2 = -angle2;
        }
        
        //is there a flip? 
        //If the determinant of either U or V (but not both) is negative,
        //a flip has occurred. Note that V == V' for determinant purposes.
        boolean flip = false;
        double detU = svd.getU().det();
        double detV = svd.getV().det();
        if((detU < 0 && detV > 0) || (detU > 0 && detV < 0)){
            flip = true;
        }
        
        //use the smaller angle to determine flip type
        int flipType;
        double angle;
        if(flip){
            if(Math.abs(angle1) > Math.abs(angle2)){
               //the smaller flip is about the Y axis
               flipType = Constants.FLIP_HORIZONTAL;
               angle = -angle2;
            }
            else{
               //the smaller flip is about the X axis
                flipType = Constants.FLIP_VERTICAL;
                angle = -angle1;
            }
        }
        else{
            //angle1 will equal angle2, so either will do
            angle = angle1;
        }
    }
    
    
}
